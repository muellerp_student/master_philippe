#Submit to cluster to gpu
FILE=$1
TAG=$2
USE_PREDICTION=$3
DATE=$(date +%d%m%Y_%H%M%S)
DATA_DIR=/srv/glusterfs/muellerp/cifar10_data/
TRAIN_STEPS=120000
NAME=${TAG}_${DATE}_${FILE}
TRAIN_HOME_DIR=/srv/glusterfs/muellerp/variant_resnet/${TAG}
TRAIN_DIR=/srv/glusterfs/muellerp/variant_resnet/${TAG}/${DATE}
BATCH_SIZE=128
BLOCKS=5

export NAME

if [ $# -lt 3 ]
  then
    echo "Please provide file name and tag for the experiment and USE_PREDICTION Flag (True or False)"
else

mkdir ${TRAIN_HOME_DIR}
mkdir ${TRAIN_DIR}
mkdir ${TRAIN_DIR}/source
cp *.py ${TRAIN_DIR}/source 


echo "submitting job to cluster..."
  qsub -N $NAME -l gpu ./pywrap_gpu.sh ${TRAIN_DIR}/source/cifar10_train.py --train_steps $TRAIN_STEPS --data_dir $DATA_DIR --train_dir $TRAIN_DIR --tag $TAG --num_residual_blocks $BLOCKS --use_prediction $USE_PREDICTION
fi
