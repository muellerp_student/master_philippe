#Submit to cluster to gpu
FILE=$1
TAG=$2
DATE=$(date +%d%m%Y_%H%M%S)
DATA_DIR=/srv/glusterfs/muellerp/cifar10_data/
TRAIN_STEPS=120000
NAME=${TAG}_${DATE}_${FILE}
TRAIN_HOME_DIR=/srv/glusterfs/muellerp/variant_resnet/${TAG}
TRAIN_DIR=/srv/glusterfs/muellerp/variant_resnet/${TAG}/${DATE}
BATCH_SIZE=128
BLOCKS=1
IS_FULL_VALIDATION=True
BIAS_INIT=$3
COMP_WEIGHT=$4
LAMBDA_INIT=$5
export NAME

if [ $# -lt 4 ]
  then
    echo "Please provide 1. file name 2. tag 3. bias init 4. comp_weight 5. lambda_init"
else

mkdir ${TRAIN_HOME_DIR}
mkdir ${TRAIN_DIR}
mkdir ${TRAIN_DIR}/source
cp *.py ${TRAIN_DIR}/source 


echo "submitting job to cluster..."
  qsub -N $NAME -l gpu ./pywrap_gpu.sh ${TRAIN_DIR}/source/cifar10_train.py --train_steps $TRAIN_STEPS --data_dir $DATA_DIR --train_dir $TRAIN_DIR --tag $TAG --num_residual_blocks $BLOCKS --is_full_validation $IS_FULL_VALIDATION --bias_init $BIAS_INIT --comp_weight $COMP_WEIGHT --lambda_init $LAMBDA_INIT
fi
