#Submit to cluster to gpu
FILE=$1
TAG=$2
DATE=$(date +%d%m%Y_%H%M%S)
DATA_DIR=/srv/glusterfs/muellerp/cifar10_data/
TRAIN_STEPS=$7
NAME=${TAG}_${DATE}_${FILE}
TRAIN_HOME_DIR=/srv/glusterfs/muellerp/variant_resnet/${TAG}
TRAIN_DIR=/srv/glusterfs/muellerp/variant_resnet/${TAG}/${DATE}
BATCH_SIZE=128
BLOCKS=1
IS_FULL_VALIDATION=True
BIAS_INIT=1.0
COMP_WEIGHT=$3
INIT_LR=$4
LR_DECAY_FACTOR=$5
SWEEP_STEPS=$6
SWEEP_TO=$8
export NAME

if [ $# -lt 8 ]
  then
    echo "Please provide 1. file name 2. tag 3. comp_weight 4. INIT_LR 5. LR_DECAY_FACTOR 6. SWEEP_STEPS 7. TRAIN_STEPS 8. SWEEP_TO"
else

mkdir ${TRAIN_HOME_DIR}
mkdir ${TRAIN_DIR}
mkdir ${TRAIN_DIR}/source
cp *.py ${TRAIN_DIR}/source 


echo "submitting job to cluster..."
  qsub -N $NAME -l gpu ./pywrap_gpu.sh ${TRAIN_DIR}/source/cifar10_train.py --train_steps $TRAIN_STEPS --data_dir $DATA_DIR --train_dir $TRAIN_DIR --tag $TAG --num_residual_blocks $BLOCKS --is_full_validation $IS_FULL_VALIDATION --bias_init $BIAS_INIT --comp_weight $COMP_WEIGHT --init_lr $INIT_LR --lr_decay_factor $LR_DECAY_FACTOR --sweep_steps $SWEEP_STEPS --train_steps $TRAIN_STEPS --sweep_to $SWEEP_TO
fi
