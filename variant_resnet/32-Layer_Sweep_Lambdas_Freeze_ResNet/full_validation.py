# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 10:25:47 2018

@author: shogun
"""
from resnet import *
from datetime import datetime

import cifar10_input 
import cifar10_train 
import tensorflow as tf

ckpt_path='/srv/glusterfs/muellerp/variant_resnet/Exp_Phil_110/08022018_131407/model.ckpt-100000'
# Initialize the Train object
train = cifar10_train.Train()

# load all training images and validation images into the memory
vali_data, vali_labels = cifar10_input.read_validation_data()

# Build the graph for train and validation
train.build_train_validation_graph()


# load model from a checkpoint

sess = tf.Session()
saver = tf.train.Saver(tf.global_variables())
saver.restore(sess, ckpt_path)
print ('Restored from checkpoint: ' + str(ckpt_path))

all_data, all_labels = cifar10_input.prepare_train_data(padding_size=2)

train_batch_data, train_batch_labels = train.generate_augment_train_batch(all_data, all_labels, 128)

        
num_batches = 10000 // 250
order = np.random.choice(10000, num_batches * FLAGS.validation_batch_size)
vali_data_subset = vali_data[order, ...]
vali_labels_subset = vali_labels[order]

loss_list = []
error_list = []

for step in range(num_batches):
    offset = step * FLAGS.validation_batch_size
    feed_dict = {train.image_placeholder: train_batch_data, train.label_placeholder: train_batch_labels,
              train.vali_image_placeholder: vali_data_subset[offset:offset+250, ...],
              train.vali_label_placeholder: vali_labels_subset[offset:offset+250],
              train.lr_placeholder: 0.1}
    loss_value, top1_error_value = session.run([loss=train.vali_loss, top1_error=train.vali_top1_error], feed_dict=feed_dict)
    loss_list.append(loss_value)
    error_list.append(top1_error_value)

validation_loss_value = np.mean(loss_list) 
validation_error_value =np.mean(error_list)        


print ('Validation top1 error = %.4f' % validation_error_value)
print ('validation_loss_value = ', validation_loss_value)
