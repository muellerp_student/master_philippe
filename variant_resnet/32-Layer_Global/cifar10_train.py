# Coder: Wenxin Xu

# Github: https://github.com/wenxinxu/resnet_in_tensorflow
# ==============================================================================

from resnet import *
from datetime import datetime
import time
import re
from cifar10_input import *
#import pandas as pd

class Train(object):
    '''
    This Object is responsible for all the training and validation process
    '''
    def __init__(self):
        # Set up all the placeholders
        self.placeholders()


    def placeholders(self):
        '''
        There are five placeholders in total.
        image_placeholder and label_placeholder are for train images and labels
        vali_image_placeholder and vali_label_placeholder are for validation imgaes and labels
        lr_placeholder is for learning rate. Feed in learning rate each time of training
        implements learning rate decay easily
        '''
        
        self.image_placeholder = tf.placeholder(dtype=tf.float32, shape=[None, IMG_HEIGHT, IMG_WIDTH, IMG_DEPTH])
        
        self.label_placeholder = tf.placeholder(dtype=tf.int32, shape=[None])

        self.vali_image_placeholder = tf.placeholder(dtype=tf.float32, shape=[None, IMG_HEIGHT, IMG_WIDTH, IMG_DEPTH])
        
        self.vali_label_placeholder = tf.placeholder(dtype=tf.int32, shape=[None])

        self.lr_placeholder = tf.placeholder(dtype=tf.float32, shape=[])
        
        self.sweep_lambda_placeholder = tf.placeholder(dtype=tf.float32, shape=(), name="sweep_lambda_placeholder")
        
#        self.image_placeholder = tf.placeholder(dtype=tf.float32,
#                                                shape=[FLAGS.train_batch_size, IMG_HEIGHT,
#                                                        IMG_WIDTH, IMG_DEPTH])
#        self.label_placeholder = tf.placeholder(dtype=tf.int32, shape=[FLAGS.train_batch_size])
#
#        self.vali_image_placeholder = tf.placeholder(dtype=tf.float32, shape=[FLAGS.validation_batch_size,
#                                                                IMG_HEIGHT, IMG_WIDTH, IMG_DEPTH])
#        self.vali_label_placeholder = tf.placeholder(dtype=tf.int32, shape=[FLAGS.validation_batch_size])
#
#        self.lr_placeholder = tf.placeholder(dtype=tf.float32, shape=[])
#        
#        self.sweep_lambda_placeholder = tf.placeholder(dtype=tf.float32, shape=(), name="sweep_lambda_placeholder")
        
        # if a profile is used, during network training all weights are used, structure is given by 
        # the profile itself

        self.sweep_lambda_value = 1.0 / FLAGS.sweep_steps

        self.sweep_lambda_value_validation = 1.0
        
        self.use_prediction_placeholder = tf.placeholder(dtype=tf.bool, name='use_prediction')
        self.apply_profile_placeholder = tf.placeholder(dtype=tf.bool, name='apply_profile')
        self.apply_profile_fc_placeholder = tf.placeholder(dtype=tf.bool, name='apply_profile_fc')
        
        self.use_prediction_value = False
        self.apply_profile_value = True
        self.apply_profile_fc_value = False
        
        self.var_list = None
        self.opt = None

    def build_train_validation_graph(self):
        '''
        This function builds the train graph and validation graph at the same time.
        '''
        
        self.global_step = tf.Variable(0, trainable=False)
        validation_step = tf.Variable(0, trainable=False)
#        self.sweep_lambda_placeholder = tf.Variable(FLAGS.sweep_lambda, trainable=False)
#        self.use_prediction_placeholder = tf.Variable(False, trainable=False)
        
        # Logits of training data and valiation data come from the same graph. The inference of
        # validation data share all the weights with train data. This is implemented by passing
        # reuse=True to the variable scopes of train graph

        logits, global_pre_logits = inference(self.image_placeholder, 
                                              self.sweep_lambda_placeholder, 
                                              self.use_prediction_placeholder, 
                                              FLAGS.num_residual_blocks, 
                                              self.apply_profile_placeholder, 
                                              self.apply_profile_fc_placeholder, 
                                              reuse=False)
        
        vali_logits, _ = inference(self.vali_image_placeholder, 
                                   self.sweep_lambda_placeholder, 
                                   self.use_prediction_placeholder, 
                                   FLAGS.num_residual_blocks, 
                                   self.apply_profile_placeholder, 
                                   self.apply_profile_fc_placeholder,  
                                   reuse=True)
   
        # The following codes calculate the train loss, which consists of the
        # softmax cross entropy and the relularization loss and the computation loss
        regu_loss_collection = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
        
        print("regu_loss collection: " + str(regu_loss_collection))
        print("trainable variables: " + str(tf.trainable_variables()))
        self.regu_loss = tf.add_n(regu_loss_collection, name="regu_loss_main")
        
        global_regu_collection = []
        for v in regu_loss_collection:
            if "global_predictor" in v.name:
                global_regu_collection.append(v)
                
        self.global_pre_regu_loss = tf.add_n(global_regu_collection, name="regu_loss_predictor")
        global_pre_loss = self.loss(global_pre_logits, self.label_placeholder)
        
        loss = self.loss(logits, self.label_placeholder)

        net_params = 0
        pred_params = 0
        print("computing total number of parameters from trainable parameters...")
        for v in tf.trainable_variables():
            nr = 0
            if "lambda" not in v.name and "beta" not in v.name and "gamma" not in v.name:
                nr = np.product([xi.value for xi in v.get_shape()])
                if "global_predictor" in v.name:
                    pred_params += nr
                    print(str(nr) + " parameters added to pred_params (" + v.name + ")" )
                    
                else:
                    net_params += nr
                    print(str(nr) + " parameters added to net_params (" + v.name + ")" )
        
        print("net_params: ", str(net_params))
        print("pred_params: ", str(pred_params))
        
        computation_effort_collection = [x if re.match(re.compile(r'conv\d_\d/'), x.name) != None else tf.zeros([1], dtype=tf.float32) for x in tf.get_collection("Layer_Computation_Effort_LOSSRELEVANT")]
#        print("computation_effort_collection: " + str(computation_effort_collection))
 
        self.train_net_actual_effort = tf.reshape( tf.add_n([x if re.match(re.compile(r'conv\d_\d/'), x.name) != None else tf.zeros([1], dtype=tf.float32) for x in tf.get_collection("ACTUAL_EFFORT")], name="nr_of_operations"), ())

        self.train_net_used_parameters_loss = tf.reshape(tf.add_n(computation_effort_collection), (), name="train_net_used_parameters_loss")
#        self.relative_used = self.train_net_used_parameters_loss / net_params
        comp_loss_collection = tf.get_collection("comp_loss")
        self.relative_used = tf.reshape(tf.add_n(comp_loss_collection, name = "dummy_add_comp_loss"), ())  
        
        self.computation_loss = tf.multiply(self.relative_used, FLAGS.comp_weight)
#        global_prediction_variance_collection = tf.get_collection("Prediction_Variance")
#        self.global_prediction_variance = tf.reshape(tf.add_n(global_prediction_variance_collection), ())
#        self.global_prediction_variance_loss = tf.multiply( 1 / (0.1 + self.global_prediction_variance), FLAGS.var_loss_weight)
        
#        print ("self.global_prediction_variance_loss: " + str(self.global_prediction_variance_loss))

#        self.computation_loss = tf.multiply(tf.add_n(tf.get_collection("Layer_Computation_Effort")), FLAGS.comp_weight, name="computation_loss_weighting")

        self.entropy_comp_regu_loss = tf.add_n([loss] + [self.computation_loss] + [self.regu_loss], name = "full_comp_loss")

        self.exponential_loss = self.entropy_comp_regu_loss#tf.exp(self.entropy_comp_regu_loss, name="exponential_loss")
        
#        print("self.entropy_comp_regu_loss.get_shape: " + str(self.entropy_comp_regu_loss.shape))
        self.full_loss  = self.entropy_comp_regu_loss #tf.add(self.entropy_comp_regu_loss, self.global_prediction_variance_loss)
        self.global_pre_full_loss = tf.add_n([global_pre_loss] + [self.global_pre_regu_loss], name="predictor_loss")
        
        predictions = tf.nn.softmax(logits, name="softmax_class_predictions_main_model")
        self.train_top1_error = self.top_k_error(predictions, self.label_placeholder, 1)
        
        global_pre_predictions = tf.nn.softmax(global_pre_logits, name="softmax_class_predictor")
        self.global_pre_train_top1_error = self.top_k_error(global_pre_predictions, self.label_placeholder, 1)
        
        # Validation loss
        self.vali_loss = self.loss(vali_logits, self.vali_label_placeholder)
        vali_predictions = tf.nn.softmax(vali_logits, name="softmax_validation")
        self.vali_top1_error = self.top_k_error(vali_predictions, self.vali_label_placeholder, 1)
        
        self.var_list = []
        
#       Train the global predictor to initialize
        
        self.global_pre_var_list = []
        for v in tf.trainable_variables():
            if 'global_predictor' in v.name and 'lambda_output' not in v.name:
                self.global_pre_var_list.append(v)
            else :
                self.var_list.append(v)

        
        self.global_pre_train_op, self.global_pre_train_ema_op = self.global_pre_train_operation(self.global_step, self.global_pre_full_loss, self.global_pre_train_top1_error, self.global_pre_var_list)
        self.train_op, self.train_ema_op = self.train_operation(self.global_step, self.full_loss, self.train_top1_error, self.train_net_actual_effort, self.var_list)

        self.val_op = self.validation_op(validation_step, self.vali_top1_error, self.vali_loss)


    def train(self):
        '''
        This is the main function for training
        '''

        # For the first step, we are loading all training images and validation images into the
        # memory
        all_data, all_labels = prepare_train_data(padding_size=FLAGS.padding_size)
        vali_data, vali_labels = read_validation_data()

        # Build the graph for train and validation
        self.build_train_validation_graph()

        # Initialize a saver to save checkpoints. Merge all summaries, so we can run all
        # summarizing operations by running summary_op. Initialize a new session
        saver = tf.train.Saver(tf.global_variables())
        summary_op = tf.summary.merge_all()
        init = tf.global_variables_initializer()
        feature_creation_vars = []
        lambda_output_init_vars = []
        for v in tf.trainable_variables():
            if 'global_predictor' not in v.name:
                continue
            else:
#                if 'prediction_conv' in v.name:
#                    feature_creation_vars.append(v)
#                if 'lambda_weights' in v.name or 'lambda_bias' in v.name:
                if 'lambda_output' in v.name:
                    lambda_output_init_vars.append(v)
        
        lambda_output_init = tf.variables_initializer(lambda_output_init_vars)
        
        sess = tf.Session()

        # If you want to load from a checkpoint
        if FLAGS.is_use_ckpt is True:
            saver.restore(sess, FLAGS.ckpt_path)
            print ('Restored from checkpoint...')
        else:
            sess.run(init)

        # This summary writer object helps write summaries on tensorboard
        summary_writer = tf.summary.FileWriter(train_dir, sess.graph)


        # These lists are used to save a csv file at last
        step_list = []
        train_error_list = []
        val_error_list = []

        print ('Start training...')
        print ('----------------------------')
        print ('Using Lambda Sweeping')

        for step in range(FLAGS.train_steps):

            train_batch_data, train_batch_labels = self.generate_augment_train_batch(all_data, all_labels,
                                                                        FLAGS.train_batch_size)


            validation_batch_data, validation_batch_labels = self.generate_vali_batch(vali_data,
                                                           vali_labels, FLAGS.validation_batch_size)

            # Want to validate once before training. You may check the theoretical validation
            # loss first
            if step % FLAGS.report_freq == 0:

                if FLAGS.is_full_validation is True:
                    validation_loss_value, validation_error_value = self.full_validation(loss=self.vali_loss,
                                            top1_error=self.vali_top1_error, vali_data=vali_data,
                                            vali_labels=vali_labels, session=sess,
                                            batch_data=train_batch_data, batch_label=train_batch_labels)

                    vali_summ = tf.Summary()
                    vali_summ.value.add(tag='full_validation_error',
                                        simple_value=validation_error_value.astype(np.float))
                    summary_writer.add_summary(vali_summ, step)
                    summary_writer.flush()

                else:
                        _, validation_error_value, validation_loss_value = sess.run(
                                        [self.val_op,
                                        self.vali_top1_error,
                                        self.vali_loss],
                                        {
                                             self.image_placeholder: train_batch_data,
                                             self.label_placeholder: train_batch_labels,
                                             self.vali_image_placeholder: validation_batch_data,
                                             self.vali_label_placeholder: validation_batch_labels,
                                             self.lr_placeholder: FLAGS.init_lr,
                                             self.sweep_lambda_placeholder: self.sweep_lambda_value_validation,
                                             self.use_prediction_placeholder: self.use_prediction_value,
                                             self.apply_profile_placeholder: self.apply_profile_value,
                                             self.apply_profile_fc_placeholder: self.apply_profile_fc_value
                                        })

                val_error_list.append(validation_error_value)

            start_time = time.time()

            _, _, train_loss_value, train_error_value, train_comp_loss = sess.run([self.train_op, 
                                                                                   self.train_ema_op,
                                                                                   self.full_loss, 
                                                                                   self.train_top1_error,
                                                                                   self.computation_loss],
                                {self.image_placeholder: train_batch_data,
                                  self.label_placeholder: train_batch_labels,
                                  self.vali_image_placeholder: validation_batch_data,
                                  self.vali_label_placeholder: validation_batch_labels,
                                  self.lr_placeholder: FLAGS.init_lr,
                                  self.sweep_lambda_placeholder: self.sweep_lambda_value,
                                  self.use_prediction_placeholder: self.use_prediction_value,
                                  self.apply_profile_placeholder: self.apply_profile_value,
                                  self.apply_profile_fc_placeholder: self.apply_profile_fc_value
                                })
    
            if step < np.floor(FLAGS.train_steps*FLAGS.sweep_to):
                _, _, global_pre_train_loss_value, global_pre_train_error_value = sess.run([self.global_pre_train_op, 
                                                                               self.global_pre_train_ema_op,
                                                                               self.global_pre_full_loss, 
                                                                               self.global_pre_train_top1_error],
                            {self.image_placeholder: train_batch_data,
                              self.label_placeholder: train_batch_labels,
                              self.vali_image_placeholder: validation_batch_data,
                              self.vali_label_placeholder: validation_batch_labels,
                              self.lr_placeholder: FLAGS.init_lr,
                              self.sweep_lambda_placeholder: self.sweep_lambda_value,
                              self.use_prediction_placeholder: self.use_prediction_value,
                              self.apply_profile_placeholder: self.apply_profile_value,
                              self.apply_profile_fc_placeholder: self.apply_profile_fc_value
                            })
    
            duration = time.time() - start_time

            if step % FLAGS.report_freq == 0:
                summary_str = sess.run(summary_op, {self.image_placeholder: train_batch_data,
                                                    self.label_placeholder: train_batch_labels,
                                                    self.vali_image_placeholder: validation_batch_data,
                                                    self.vali_label_placeholder: validation_batch_labels,
                                                    self.lr_placeholder: FLAGS.init_lr,
                                                    self.sweep_lambda_placeholder: self.sweep_lambda_value,
                                                    self.use_prediction_placeholder: self.use_prediction_value,
                                                    self.apply_profile_placeholder: self.apply_profile_value,
                                                    self.apply_profile_fc_placeholder: self.apply_profile_fc_value
                                                    })
                summary_writer.add_summary(summary_str, step)

                num_examples_per_step = FLAGS.train_batch_size
                examples_per_sec = num_examples_per_step / duration
                sec_per_batch = float(duration)
                
                format_str = ('%s: step %d, loss = %.4f (%.1f examples/sec; %.3f ' 'sec/batch)')
                print (format_str % (datetime.now(), step, train_loss_value, examples_per_sec,
                                    sec_per_batch))
                print ('Train Computation loss = %.4f' % train_comp_loss)
                print ('Train top1 error = ', train_error_value)
                print ('Global Pre Train top1 error = ', global_pre_train_error_value)
                print ('global_pre_train_loss_value = ', global_pre_train_loss_value)
                print ('Validation top1 error = %.4f' % validation_error_value)
                print ('validation_loss_value = ', validation_loss_value)

                step_list.append(step)
                train_error_list.append(train_error_value)

            if step == FLAGS.decay_step0 or step == FLAGS.decay_step1:
                FLAGS.init_lr = FLAGS.lr_decay_factor * FLAGS.init_lr
                print ('Learning rate decayed to ', FLAGS.init_lr)
            
            if FLAGS.sweep_steps > 1.0:
                sweep_steps = FLAGS.sweep_steps
                for i in range(sweep_steps - 1):
                    factor = (i + 1)/sweep_steps
                    if step == np.floor(FLAGS.train_steps*FLAGS.sweep_to*factor):
                       self.sweep_lambda_value = factor + 1.0 / sweep_steps
                       break

            if step == np.floor(FLAGS.train_steps*FLAGS.sweep_to):
                self.use_prediction_value = True
                FLAGS.init_lr = FLAGS.decay_on_finetune*FLAGS.init_lr
                FLAGS.train_batch_size = FLAGS.finetune_batch_size
#                FLAGS.validation_batch_size = FLAGS.finetune_batch_size
                print("INITIALIZING LAMBDA_OUTPUT_VARS")
                sess.run(lambda_output_init)
                if FLAGS.joint_training_after_sweep == False:
                    self.var_list = []
                    for v in tf.trainable_variables():
#                        if 'global_predictor' in v.name and "class_output" not in v.name:
                        if 'lambda_output' in v.name:
                            print("MATCH: building finetune var_list: " + v.name )
                            self.var_list.append(v)
                        else:
                            continue
                else:
                    self.var_list = []
                    for v in tf.trainable_variables():
#                        if 'global_predictor' in v.name and "class_output" not in v.name:
                        if 'class_output' not in v.name:
                            print("MATCH: building finetune var_list: " + v.name )
                            self.var_list.append(v)
                        else:
                            continue                    
#                 Freeze Network Here
#                self.var_list = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='*/prediction')
                print ("collections: " + str(tf.get_default_graph().get_all_collection_keys()))
                
#                self.var_list = tf.get_collection('predictor').append(tf.get_collection('feature_creation'))         
#                print ("var_list: " + str(self.var_list))
                self.train_op, _ = self.train_operation(self.global_step, self.exponential_loss, self.train_top1_error, self.train_net_actual_effort, self.var_list)
                
            # Save checkpoints every 10000 steps
            if step % 10000 == 0 or (step + 1) == FLAGS.train_steps:
                checkpoint_path = os.path.join(train_dir, 'model.ckpt')
                saver.save(sess, checkpoint_path, global_step=step)
                
            if step == FLAGS.train_steps - 1:
                self.use_prediction_value = False
#                self.apply_profile_value = False
#                self.apply_profile_fc_value = False
                val_sweep_steps = 10
                factor = 1.0 / 10
                
                for i in range(val_sweep_steps):
                    self.sweep_lambda_value_validation = (i+1)*factor
                        
                    validation_loss_value, validation_error_value = self.full_validation(loss=self.vali_loss,
                                            top1_error=self.vali_top1_error, vali_data=vali_data,
                                            vali_labels=vali_labels, session=sess,
                                            batch_data=train_batch_data, batch_label=train_batch_labels)        
                    val_str = ('%s: step %d, sweep_value: %.2f, full validation error: %.4f')
                    print (val_str % (datetime.now(), step, self.sweep_lambda_value_validation, validation_error_value))
                
#                df = pd.DataFrame(data={'step':step_list, 'train_error':train_error_list,
#                                'validation_error': val_error_list})
#                df.to_csv(train_dir + FLAGS.version + '_error.csv')


    ## Helper functions
    def loss(self, logits, labels):
        '''
        Calculate the cross entropy loss given logits and true labels
        :param logits: 2D tensor with shape [batch_size, num_labels]
        :param labels: 1D tensor with shape [batch_size]
        :return: loss tensor with shape [1]
        '''
        labels = tf.cast(labels, tf.int64)
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits,
                                                                       labels=labels, name='cross_entropy_per_example')
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
        return cross_entropy_mean


    def top_k_error(self, predictions, labels, k):
        '''
        Calculate the top-k error
        :param predictions: 2D tensor with shape [batch_size, num_labels]
        :param labels: 1D tensor with shape [batch_size, 1]
        :param k: int
        :return: tensor with shape [1]
        '''
        batch_size = tf.shape(predictions)[0]
        batch_size = tf.cast(batch_size, tf.float32)
#        batch_size = predictions.get_shape().as_list()[0]
        in_top1 = tf.to_float(tf.nn.in_top_k(predictions, labels, k=1))
        num_correct = tf.reduce_sum(in_top1)
        return tf.divide(tf.subtract(batch_size , num_correct) , batch_size)


    def generate_vali_batch(self, vali_data, vali_label, vali_batch_size):
        '''
        If you want to use a random batch of validation data to validate instead of using the
        whole validation data, this function helps you generate that batch
        :param vali_data: 4D numpy array
        :param vali_label: 1D numpy array
        :param vali_batch_size: int
        :return: 4D numpy array and 1D numpy array
        '''
        offset = np.random.choice(10000 - vali_batch_size, 1)[0]
        vali_data_batch = vali_data[offset:offset+vali_batch_size, ...]
        vali_label_batch = vali_label[offset:offset+vali_batch_size]
        return vali_data_batch, vali_label_batch


    def generate_augment_train_batch(self, train_data, train_labels, train_batch_size):
        '''
        This function helps generate a batch of train data, and random crop, horizontally flip
        and whiten them at the same time
        :param train_data: 4D numpy array
        :param train_labels: 1D numpy array
        :param train_batch_size: int
        :return: augmented train batch data and labels. 4D numpy array and 1D numpy array
        '''
        offset = np.random.choice(EPOCH_SIZE - train_batch_size, 1)[0]
        batch_data = train_data[offset:offset+train_batch_size, ...]
        batch_data = random_crop_and_flip(batch_data, padding_size=FLAGS.padding_size)

        batch_data = whitening_image(batch_data)
        batch_label = train_labels[offset:offset+FLAGS.train_batch_size]

        return batch_data, batch_label


    def train_operation(self, global_step, total_loss, top1_error, train_net_actual_effort, var_list = None):
        '''
        Defines train operations
        :param global_step: tensor variable with shape [1]
        :param total_loss: tensor with shape [1]
        :param top1_error: tensor with shape [1]
        :return: two operations. Running train_op will do optimization once. Running train_ema_op
        will generate the moving average of train error and train loss for tensorboard
        '''
        # Add train_loss, current learning rate and train error into the tensorboard summary ops
        tf.summary.scalar('learning_rate', self.lr_placeholder)
        tf.summary.scalar('computation_loss', self.computation_loss)
        tf.summary.scalar('relative_used', self.relative_used)                        
        tf.summary.scalar('train_net_used_parameters', self.train_net_used_parameters_loss)
        tf.summary.scalar('train_net_actual_effort', train_net_actual_effort)
        tf.summary.scalar('train_loss', total_loss)
        tf.summary.scalar('train_top1_error', top1_error)
        tf.summary.scalar('regu_loss', self.regu_loss)
#        tf.summary.scalar('global_prediction_variance', self.global_prediction_variance)
#        tf.summary.scalar('global_prediction_variance_loss', self.global_prediction_variance_loss)
        
        print("train_operation called with var_list to optimize: " + str(var_list))

        # The ema object help calculate the moving average of train loss and train error
        ema = tf.train.ExponentialMovingAverage(FLAGS.train_ema_decay, global_step)
        train_ema_op = ema.apply([total_loss, top1_error])
        tf.summary.scalar('train_top1_error_avg', ema.average(top1_error))
        tf.summary.scalar('train_loss_avg', ema.average(total_loss))

#        self.opt = tf.cond(self.opt is None, lambda: tf.train.MomentumOptimizer(learning_rate=self.lr_placeholder, momentum=0.9), lambda: self.opt)
        if self.opt == None:
            self.opt = tf.train.MomentumOptimizer(learning_rate=self.lr_placeholder, momentum=0.9)
        else:
            self.opt = tf.train.GradientDescentOptimizer(learning_rate=self.lr_placeholder)

        
        train_op = self.opt.minimize(total_loss, global_step=global_step, var_list=var_list)
        return train_op, train_ema_op
    
    def global_pre_train_operation(self, global_step, total_loss, top1_error, var_list):
        '''
        Defines train operations
        :param global_step: tensor variable with shape [1]
        :param total_loss: tensor with shape [1]
        :param top1_error: tensor with shape [1]
        :return: two operations. Running train_op will do optimization once. Running train_ema_op
        will generate the moving average of train error and train loss for tensorboard
        '''
        # Add train_loss, current learning rate and train error into the tensorboard summary ops
        tf.summary.scalar('global_pre_train_loss', total_loss)
        tf.summary.scalar('global_pre_train_top1_error', top1_error)
#        tf.summary.scalar('global_prediction_variance', self.global_prediction_variance)
#        tf.summary.scalar('global_prediction_variance_loss', self.global_prediction_variance_loss)
        
        print("global_pre_train_operation called with var_list to optimize: " + str(var_list))

        # The ema object help calculate the moving average of train loss and train error
        ema = tf.train.ExponentialMovingAverage(FLAGS.train_ema_decay, global_step)
        train_ema_op = ema.apply([total_loss, top1_error])
        tf.summary.scalar('train_top1_error_avg', ema.average(top1_error))
        tf.summary.scalar('train_loss_avg', ema.average(total_loss))

#        self.opt = tf.cond(self.opt is None, lambda: tf.train.MomentumOptimizer(learning_rate=self.lr_placeholder, momentum=0.9), lambda: self.opt)
        opt = tf.train.MomentumOptimizer(learning_rate=self.lr_placeholder, momentum=0.9)

        
        train_op = opt.minimize(total_loss, global_step=global_step, var_list=var_list)
        return train_op, train_ema_op
    
    def validation_op(self, validation_step, top1_error, loss):
        '''
        Defines validation operations
        :param validation_step: tensor with shape [1]
        :param top1_error: tensor with shape [1]
        :param loss: tensor with shape [1]
        :return: validation operation
        '''

        # This ema object help calculate the moving average of validation loss and error

        # ema with decay = 0.0 won't average things at all. This returns the original error
        ema = tf.train.ExponentialMovingAverage(0.0, validation_step)
        ema2 = tf.train.ExponentialMovingAverage(0.95, validation_step)


        val_op = tf.group(validation_step.assign_add(1), ema.apply([top1_error, loss]),
                          ema2.apply([top1_error, loss]))
        top1_error_val = ema.average(top1_error)
        top1_error_avg = ema2.average(top1_error)
        loss_val = ema.average(loss)
        loss_val_avg = ema2.average(loss)

        # Summarize these values on tensorboard
        tf.summary.scalar('val_top1_error', top1_error_val)
        tf.summary.scalar('val_top1_error_avg', top1_error_avg)
        tf.summary.scalar('val_loss', loss_val)
        tf.summary.scalar('val_loss_avg', loss_val_avg)
        return val_op

    def validate_sweep_from_trained_model(self):
        # For the first step, we are loading all training images and validation images into the
        # memory
        all_data, all_labels = prepare_train_data(padding_size=FLAGS.padding_size)
        vali_data, vali_labels = read_validation_data()

        # Build the graph for train and validation
        self.build_train_validation_graph()

        # Initialize a saver to save checkpoints. Merge all summaries, so we can run all
        # summarizing operations by running summary_op. Initialize a new session
        print("self.checkpoint_path: " + str(self.checkpoint_path))
        latest_model = tf.train.latest_checkpoint(self.checkpoint_path)
        print("latest_model: " + str(latest_model))

#        name_of_meta_file = latest_model + ".meta"
#        saver = tf.train.import_meta_graph(name_of_meta_file)
        saver = tf.train.Saver(tf.global_variables())
        
        summary_op = tf.summary.merge_all()
        sess = tf.Session()

        # If you want to load from a checkpoint
        FLAGS.is_use_ckpt = True
        if FLAGS.is_use_ckpt is True:
            saver.restore(sess, latest_model)
            print ('Restored from checkpoint...')
        else:
            sess.run(init)

        # This summary writer object helps write summaries on tensorboard
        summary_writer = tf.summary.FileWriter(train_dir, sess.graph)
        self.use_prediction_value = False

        # These lists are used to save a csv file at last
        step_list = []
        sweep_steps = FLAGS.sweep_steps

        print ('Start evaluating model')
        print ('----------------------------')
        print ('Using %d Lambda Sweep Steps' % sweep_steps)
        
        factor = 1.0 / sweep_steps
        for i in range(sweep_steps):
            train_batch_data, train_batch_labels = self.generate_augment_train_batch(all_data, all_labels,
                                                                        FLAGS.train_batch_size)

            validation_batch_data, validation_batch_labels = self.generate_vali_batch(vali_data,
                                                           vali_labels, FLAGS.validation_batch_size)
           
            self.sweep_lambda_value_validation = (i + 1) * factor
                
            validation_loss_value, validation_error_value = self.full_validation(loss=self.vali_loss,
                                    top1_error=self.vali_top1_error, vali_data=vali_data,
                                    vali_labels=vali_labels, session=sess,
                                    batch_data=train_batch_data, batch_label=train_batch_labels)        
            val_str = ('%s: sweep_value: %.2f, full validation error: %.4f')
            print (val_str % (datetime.now(), self.sweep_lambda_value_validation, validation_error_value))

    def full_validation(self, loss, top1_error, session, vali_data, vali_labels, batch_data,
                        batch_label):
        '''
        Runs validation on all the 10000 valdiation images
        :param loss: tensor with shape [1]
        :param top1_error: tensor with shape [1]
        :param session: the current tensorflow session
        :param vali_data: 4D numpy array
        :param vali_labels: 1D numpy array
        :param batch_data: 4D numpy array. training batch to feed dict and fetch the weights
        :param batch_label: 1D numpy array. training labels to feed the dict
        :return: float, float
        '''
        num_batches = 10000 // FLAGS.validation_batch_size
        order = np.random.choice(10000, num_batches * FLAGS.validation_batch_size)
        vali_data_subset = vali_data[order, ...]
        vali_labels_subset = vali_labels[order]

        loss_list = []
        error_list = []

        for step in range(num_batches):
            offset = step * FLAGS.validation_batch_size
            feed_dict = {self.image_placeholder: batch_data, self.label_placeholder: batch_label,
                self.vali_image_placeholder: vali_data_subset[offset:offset+FLAGS.validation_batch_size, ...],
                self.vali_label_placeholder: vali_labels_subset[offset:offset+FLAGS.validation_batch_size],
                self.lr_placeholder: FLAGS.init_lr,
                self.sweep_lambda_placeholder: self.sweep_lambda_value_validation,
                self.use_prediction_placeholder: self.use_prediction_value,
                self.apply_profile_placeholder: self.apply_profile_value,
                self.apply_profile_fc_placeholder: self.apply_profile_fc_value}
            loss_value, top1_error_value = session.run([loss, top1_error], feed_dict=feed_dict)
            loss_list.append(loss_value)
            error_list.append(top1_error_value)

        return np.mean(loss_list), np.mean(error_list)
    
# maybe_download_and_extract()
# Initialize the Train object
train = Train()

# Start the training session
train.train()
#train.validate_sweep_from_trained_model()

