#!/bin/bash
#$ -l h_rt=8:00:00
#$ -l h_vmem=40G
#$ -l gpu=1
#$ -l h="biwirender0[5-8]|biwirender[1-9][0-9]"
#$ -cwd
#$ -o /srv/glusterfs/muellerp/variant_resnet/logs
#$ -V
#$ -j y
#Usage: ./pywrap.sh filename.py --lr 0.000001 etc
echo "Arguments given to pywrap.sh :"
echo "$@"
source /srv/glusterfs/muellerp/gpu_tensorflow35/bin/activate
export CUDA_VISIBLE_DEVICES=$SGE_GPU
echo "CUDA_VISIBLE_DEVICES in pywrap.sh: $CUDA_VISIBLE_DEVICES"
#BRANCH=$1
#shift 1
#BASE=/scratch/aeirikur/hashnets_experiments
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE} )
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/projects/hashnets/ .
python "$@"
