#!/usr/bin/env python
import sys
if hasattr(sys, 'real_prefix'):
	print "Executed by virtualenv: " + sys.prefix
	import tensorflow as tf
	print "Tensorflow version: " + tf.__version__ 
else:
	print "executed by global env: " + sys.prefix
	sys.exit(0)
hello = tf.constant('Hello, This is TensorFlow Running')
sess = tf.Session()
print(sess.run(hello))
