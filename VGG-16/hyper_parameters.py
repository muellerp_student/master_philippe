# Coder: Wenxin Xu
# Github: https://github.com/wenxinxu/resnet_in_tensorflow
# ==============================================================================
import tensorflow as tf

FLAGS = tf.app.flags.FLAGS

## The following flags are related to save paths, tensorboard outputs and screen outputs
tf.app.flags.DEFINE_string('version', 'freeze_sweep_8steps_500epochs_no_freeze', '''A version number defining the directory to save logs and checkpoints''')

tf.app.flags.DEFINE_string('data_dir', '../variant_resnet/cifar10_data/', '''Data directory''')
tf.app.flags.DEFINE_string('train_dir', '', '''Directory to store checkpoints''')

tf.app.flags.DEFINE_integer('report_freq', 20, '''Steps takes to output errors on the screen
and write summaries''')
tf.app.flags.DEFINE_float('train_ema_decay', 0.95, '''The decay factor of the train error's
moving average shown on tensorboard''')


## The following flags define hyper-parameters regards training
tf.app.flags.DEFINE_integer('train_steps', 100000, '''Total steps that you want to train''')
tf.app.flags.DEFINE_boolean('is_full_validation', True, '''Validation w/ full validation set or
a random batch''')
tf.app.flags.DEFINE_float('init_percent', 0.1, '''initialize before applying profile and sweep''')


tf.app.flags.DEFINE_integer('train_batch_size', 128, '''Train batch size''')
tf.app.flags.DEFINE_integer('validation_batch_size', 128, '''Validation batch size, better to be a divisor of 10000 for this task''')
tf.app.flags.DEFINE_integer('test_batch_size', 125, '''Test batch size''')


tf.app.flags.DEFINE_integer('sweep_steps', 8, '''sweep lambda how many steps''')
tf.app.flags.DEFINE_float('sweep_to', 1.0, '''lambda sweep is applied until this fraction of train steps, afterwards computation loss is added and prediction layers are trained''')

tf.app.flags.DEFINE_boolean('sweep_freeze', False, '''freeze the sweep weights''')
tf.app.flags.DEFINE_boolean('apply_on_input', True, '''set this flag for profiles and sweeps on input''')
tf.app.flags.DEFINE_boolean('use_bn', False, '''whether BN should be used''')

tf.app.flags.DEFINE_string('profile_type', 'linear', '''Choose from: harmonic, linear, all-ones''')


tf.app.flags.DEFINE_float('init_lr', 0.1, '''Initial learning rate''')
tf.app.flags.DEFINE_float('lr_decay_factor', 0.1, '''How much to decay the learning rate each
time''')
tf.app.flags.DEFINE_integer('decay_step0', 20000, '''At which step to decay the learning rate''')
tf.app.flags.DEFINE_integer('decay_step1', 60000, '''At which step to decay the learning rate''')


## The following flags define hyper-parameters modifying the training network

tf.app.flags.DEFINE_float('weight_decay', 0.0002, '''scale for l2 regularization''')
tf.app.flags.DEFINE_float('bias_init', 0.01, '''bias init''')


## If you want to load a checkpoint and continue training
tf.app.flags.DEFINE_string('ckpt_path', 'cache/logs_repeat20/model.ckpt-100000', '''Checkpoint
directory to restore''')
tf.app.flags.DEFINE_boolean('is_use_ckpt', False, '''Whether to load a checkpoint and continue
training''')


tf.app.flags.DEFINE_string('test_ckpt_path', 'model_110.ckpt-79999', '''Checkpoint
directory to restore''')
## The following flags are related to data-augmentation
tf.app.flags.DEFINE_integer('padding_size', 2, '''In data augmentation, layers of zero padding on
each side of the image''')

train_dir = FLAGS.train_dir + '/logs_' + FLAGS.version + '/'
data_dir = FLAGS.data_dir
