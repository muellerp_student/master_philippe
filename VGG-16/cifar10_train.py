# Coder: Wenxin Xu

# Github: https://github.com/wenxinxu/resnet_in_tensorflow
# ==============================================================================

from VGG16 import *
from datetime import datetime
import time
import re
#import pandas as pd
from cifar10_input import *


def get_trailing_number(s):
    m = re.search(r'\d+$', s)
    return int(m.group()) if m else None

class Train(object):
    '''
    This Object is responsible for all the training and validation process
    '''
    def __init__(self):
        # Set up all the placeholders
        self.placeholders()


    def placeholders(self):
        '''
        There are five placeholders in total.
        image_placeholder and label_placeholder are for train images and labels
        vali_image_placeholder and vali_label_placeholder are for validation imgaes and labels
        lr_placeholder is for learning rate. Feed in learning rate each time of training
        implements learning rate decay easily
        '''
        
        self.image_placeholder = tf.placeholder(dtype=tf.float32, shape=[FLAGS.train_batch_size, IMG_HEIGHT, IMG_WIDTH, IMG_DEPTH])
        
        self.label_placeholder = tf.placeholder(dtype=tf.int32, shape=[FLAGS.train_batch_size])

        self.vali_image_placeholder = tf.placeholder(dtype=tf.float32, shape=[FLAGS.validation_batch_size, IMG_HEIGHT, IMG_WIDTH, IMG_DEPTH])
        
        self.vali_label_placeholder = tf.placeholder(dtype=tf.int32, shape=[FLAGS.validation_batch_size])

        self.lr_placeholder = tf.placeholder(dtype=tf.float32, shape=[])
        
        self.sweep_lambda_placeholder = tf.placeholder(dtype=tf.float32, shape=(), name="sweep_lambda_placeholder")
        
        self.sweep_lambda_value = 1.0 #/ FLAGS.sweep_steps
        self.sweep_lambda_value_validation = 1.0
        
        self.is_init_placeholder = tf.placeholder(dtype=tf.bool, name='is_init')        
        self.is_init_value = True
        
    def build_train_validation_graph(self):
        '''
        This function builds the train graph and validation graph at the same time.
        '''
        
        self.global_step = tf.Variable(0, trainable=False)
        validation_step = tf.Variable(0, trainable=False)

        logits = inference(self.image_placeholder, self.sweep_lambda_placeholder, self.is_init_placeholder, reuse=False)
        
        vali_logits = inference(self.vali_image_placeholder, self.sweep_lambda_placeholder, self.is_init_placeholder, reuse=True)
   
        # The following codes calculate the train loss, which consists of the
        # softmax cross entropy and the relularization loss and the computation loss
        regu_loss_collection = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
        
        self.regu_loss = tf.add_n(regu_loss_collection)
        loss = self.loss(logits, self.label_placeholder)

        self.full_loss = tf.add_n([loss] + [self.regu_loss])

        predictions = tf.nn.softmax(logits)
        self.train_top1_error = self.top_k_error(predictions, self.label_placeholder, 1)
                
        # Validation loss
        self.vali_loss = self.loss(vali_logits, self.vali_label_placeholder)
        vali_predictions = tf.nn.softmax(vali_logits)
        self.vali_top1_error = self.top_k_error(vali_predictions, self.vali_label_placeholder, 1)
        
        self.opt = None
        var_list = None
        
        self.train_op, self.train_ema_op = self.train_operation(self.global_step, self.full_loss, self.train_top1_error, var_list)
        self.val_op = self.validation_op(validation_step, self.vali_top1_error, self.vali_loss)


    def train(self):
        '''
        This is the main function for training
        '''

        # For the first step, we are loading all training images and validation images into the
        # memory
        all_data, all_labels = prepare_train_data(padding_size=FLAGS.padding_size)
        vali_data, vali_labels = read_validation_data()
        
        # Build the graph for train and validation
        self.build_train_validation_graph()

        # Initialize a saver to save checkpoints. Merge all summaries, so we can run all
        # summarizing operations by running summary_op. Initialize a new session
        saver = tf.train.Saver(tf.global_variables())
        summary_op = tf.summary.merge_all()
        init = tf.global_variables_initializer()

        sess = tf.Session()

        # If you want to load from a checkpoint
        if FLAGS.is_use_ckpt is True:
            saver.restore(sess, FLAGS.ckpt_path)
            print ('Restored from checkpoint...')
        else:
            sess.run(init)

        # This summary writer object helps write summaries on tensorboard
        summary_writer = tf.summary.FileWriter(train_dir, sess.graph)


        # These lists are used to save a csv file at last
        step_list = []
        train_error_list = []
        val_error_list = []

        print ('Start training...')
        print ('----------------------------')

        for step in np.arange(-int(FLAGS.train_steps*FLAGS.init_percent), FLAGS.train_steps):
            
            if step == 0:
                self.is_init_value = False
                if FLAGS.sweep_steps > 1.0:
                    self.sweep_lambda_value = 1.0 / FLAGS.sweep_steps
                
            train_batch_data, train_batch_labels = self.generate_augment_train_batch(all_data, all_labels,
                                                                        FLAGS.train_batch_size)


            validation_batch_data, validation_batch_labels = self.generate_vali_batch(vali_data,
                                                           vali_labels, FLAGS.validation_batch_size)

            # Want to validate once before training. You may check the theoretical validation
            # loss first
            if step % FLAGS.report_freq == 0:

                if FLAGS.is_full_validation is True:
                    validation_loss_value, validation_error_value = self.full_validation(
                                            loss=self.vali_loss,
                                            top1_error=self.vali_top1_error, 
                                            vali_data=vali_data,
                                            vali_labels=vali_labels, 
                                            session=sess,
                                            batch_data=train_batch_data, 
                                            batch_label=train_batch_labels)

                    vali_summ = tf.Summary()
                    vali_summ.value.add(tag='full_validation_error',
                                        simple_value=validation_error_value.astype(np.float))
                    summary_writer.add_summary(vali_summ, step)
                    summary_writer.flush()

                else:
                        _, validation_error_value, validation_loss_value = sess.run(
                                        [self.val_op,
                                        self.vali_top1_error,
                                        self.vali_loss],
                                        {
                                             self.image_placeholder: train_batch_data,
                                             self.label_placeholder: train_batch_labels,
                                             self.vali_image_placeholder: validation_batch_data,
                                             self.vali_label_placeholder: validation_batch_labels,
                                             self.lr_placeholder: FLAGS.init_lr,
                                             self.sweep_lambda_placeholder: self.sweep_lambda_value_validation,
                                             self.is_init_placeholder: self.is_init_value,
                                        })

                val_error_list.append(validation_error_value)

            start_time = time.time()

            _, _, train_loss_value, train_error_value = sess.run([self.train_op, 
                                                                                   self.train_ema_op,
                                                                                   self.full_loss, 
                                                                                   self.train_top1_error],
                                {self.image_placeholder: train_batch_data,
                                  self.label_placeholder: train_batch_labels,
                                  self.vali_image_placeholder: validation_batch_data,
                                  self.vali_label_placeholder: validation_batch_labels,
                                  self.lr_placeholder: FLAGS.init_lr,
                                  self.sweep_lambda_placeholder: self.sweep_lambda_value,
                                  self.is_init_placeholder: self.is_init_value
                                })
    
            duration = time.time() - start_time

            if step % ( FLAGS.train_steps // 50 ) == 0:
                summary_str = sess.run(summary_op, {self.image_placeholder: train_batch_data,
                                                    self.label_placeholder: train_batch_labels,
                                                    self.vali_image_placeholder: validation_batch_data,
                                                    self.vali_label_placeholder: validation_batch_labels,
                                                    self.lr_placeholder: FLAGS.init_lr,
                                                    self.sweep_lambda_placeholder: self.sweep_lambda_value,
                                                    self.is_init_placeholder: self.is_init_value
                                                    })
                summary_writer.add_summary(summary_str, step)

                num_examples_per_step = FLAGS.train_batch_size
                examples_per_sec = num_examples_per_step / (duration)
                sec_per_batch = float(duration)
                
                format_str = ('%s: step %d, loss = %.4f (%.1f examples/sec; %.3f ' 'sec/batch)')
                print (format_str % (datetime.now(), step, train_loss_value, examples_per_sec,
                                    sec_per_batch))
                print ('Train top1 error = ', train_error_value)
                print ('Validation top1 error = %.4f' % validation_error_value)
                print ('validation_loss_value = ', validation_loss_value)

                step_list.append(step)
                train_error_list.append(train_error_value)
                
            if step == FLAGS.decay_step0 or step == FLAGS.decay_step1:
                FLAGS.init_lr = FLAGS.lr_decay_factor * FLAGS.init_lr
                print ('Learning rate decayed to ', FLAGS.init_lr)
            
            if FLAGS.sweep_steps > 1.0:
                sweep_steps = FLAGS.sweep_steps
                for i in range(sweep_steps - 1):
                    factor = (i + 1)/sweep_steps
                    if step == np.floor(FLAGS.train_steps*FLAGS.sweep_to*factor):
                       self.sweep_lambda_value = factor + 1.0 / sweep_steps
                       print("############## SWEEP: %2.2f" % self.sweep_lambda_value)
                       if FLAGS.sweep_freeze == True:
                           print("############ building new var_list")
                           var_list = []
                           for v in tf.get_collection("IDP_CHANNEL_WEIGHTS"):   
                               v_index = get_trailing_number(v.name[:-2])
                               v_nr_channels = v.get_shape().as_list()[0]
                               nr_of_channels_in_sweep_step = v_nr_channels / sweep_steps
                               if (v_index + 1) <= np.ceil((i + 2) * nr_of_channels_in_sweep_step) and (v_index + 1) > np.floor((i+1)  * nr_of_channels_in_sweep_step):
                                   var_list.append(v)
                                   print("MATCH: " + str(v.name))
                           self.train_op, _ = self.train_operation(self.global_step, self.full_loss, self.train_top1_error, var_list)
                        
                       break

                
            # Save checkpoints every 10000 steps
            if step % 10000 == 0 or (step + 1) == FLAGS.train_steps:
                checkpoint_path = os.path.join(train_dir, 'model.ckpt')
                saver.save(sess, checkpoint_path, global_step=step)
                
            if step == FLAGS.train_steps - 1:
                val_sweep_steps = 16
                factor = 1.0 / val_sweep_steps
                
                for i in range(val_sweep_steps):
                    self.sweep_lambda_value_validation = (i+1) * factor
                        
                    validation_loss_value, validation_error_value = self.full_validation(loss=self.vali_loss,
                                            top1_error=self.vali_top1_error,
                                            vali_data=vali_data,
                                            vali_labels=vali_labels,
                                            session=sess,
                                            batch_data=train_batch_data, batch_label=train_batch_labels)        
                    val_str = ('%s: step %d, sweep_value: %.2f, full validation error: %.4f')
                    print (val_str % (datetime.now(), step, self.sweep_lambda_value_validation, validation_error_value))
                
#                df = pd.DataFrame(data={'step':step_list, 'train_error':train_error_list,
#                                'validation_error': val_error_list})
#                df.to_csv(train_dir + FLAGS.version + '_error.csv')


    ## Helper functions
    def loss(self, logits, labels):
        '''
        Calculate the cross entropy loss given logits and true labels
        :param logits: 2D tensor with shape [batch_size, num_labels]
        :param labels: 1D tensor with shape [batch_size]
        :return: loss tensor with shape [1]
        '''
        labels = tf.cast(labels, tf.int64)
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits,
                                                                       labels=labels, name='cross_entropy_per_example')
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
        return cross_entropy_mean


    def top_k_error(self, predictions, labels, k):
        '''
        Calculate the top-k error
        :param predictions: 2D tensor with shape [batch_size, num_labels]
        :param labels: 1D tensor with shape [batch_size, 1]
        :param k: int
        :return: tensor with shape [1]
        '''
        batch_size = tf.shape(predictions)[0]
        batch_size = tf.cast(batch_size, tf.float32)
#        batch_size = predictions.get_shape().as_list()[0]
        in_top1 = tf.to_float(tf.nn.in_top_k(predictions, labels, k=1))
        num_correct = tf.reduce_sum(in_top1)
        return tf.divide(tf.subtract(batch_size , num_correct) , batch_size)

    def generate_vali_batch(self, vali_data, vali_label, vali_batch_size):
        '''
        If you want to use a random batch of validation data to validate instead of using the
        whole validation data, this function helps you generate that batch
        :param vali_data: 4D numpy array
        :param vali_label: 1D numpy array
        :param vali_batch_size: int
        :return: 4D numpy array and 1D numpy array
        '''
        offset = np.random.choice(10000 - vali_batch_size, 1)[0]
        vali_data_batch = vali_data[offset:offset+vali_batch_size, ...]
        vali_label_batch = vali_label[offset:offset+vali_batch_size]
        return vali_data_batch, vali_label_batch


    def generate_augment_train_batch(self, train_data, train_labels, train_batch_size):
        '''
        This function helps generate a batch of train data, and random crop, horizontally flip
        and whiten them at the same time
        :param train_data: 4D numpy array
        :param train_labels: 1D numpy array
        :param train_batch_size: int
        :return: augmented train batch data and labels. 4D numpy array and 1D numpy array
        '''
        offset = np.random.choice(EPOCH_SIZE - train_batch_size, 1)[0]
        batch_data = train_data[offset:offset+train_batch_size, ...]
        batch_data = random_crop_and_flip(batch_data, padding_size=FLAGS.padding_size)

        batch_data = whitening_image(batch_data)
        batch_label = train_labels[offset:offset+FLAGS.train_batch_size]

        return batch_data, batch_label


    def train_operation(self, global_step, total_loss, top1_error, var_list = None):
        '''
        Defines train operations
        :param global_step: tensor variable with shape [1]
        :param total_loss: tensor with shape [1]
        :param top1_error: tensor with shape [1]
        :return: two operations. Running train_op will do optimization once. Running train_ema_op
        will generate the moving average of train error and train loss for tensorboard
        '''
        # Add train_loss, current learning rate and train error into the tensorboard summary ops
        tf.summary.scalar('learning_rate', self.lr_placeholder)
        tf.summary.scalar('train_loss', total_loss)
        tf.summary.scalar('train_top1_error', top1_error)
        
        # The ema object help calculate the moving average of train loss and train error
        ema = tf.train.ExponentialMovingAverage(FLAGS.train_ema_decay, global_step)
        train_ema_op = ema.apply([total_loss, top1_error])
        tf.summary.scalar('train_top1_error_avg', ema.average(top1_error))
        tf.summary.scalar('train_loss_avg', ema.average(total_loss))
        
        if self.opt == None:
            print("####### creating optimizer")
#            self.opt = tf.train.AdamOptimizer(learning_rate = self.lr_placeholder)
            self.opt = tf.train.MomentumOptimizer(learning_rate=self.lr_placeholder, momentum=0.9, use_nesterov=True)
        
        print("@@@@@@@@@@@@@@@@@@@@ train_operation called with var_list:")
        if var_list:
            for var in var_list:
                print(var.name)
                
        train_op = self.opt.minimize(total_loss, global_step=global_step, var_list=var_list)
        return train_op, train_ema_op
    
    
    def validation_op(self, validation_step, top1_error, loss):
        '''
        Defines validation operations
        :param validation_step: tensor with shape [1]
        :param top1_error: tensor with shape [1]
        :param loss: tensor with shape [1]
        :return: validation operation
        '''

        # This ema object help calculate the moving average of validation loss and error

        # ema with decay = 0.0 won't average things at all. This returns the original error
        ema = tf.train.ExponentialMovingAverage(0.0, validation_step)
        ema2 = tf.train.ExponentialMovingAverage(0.95, validation_step)


        val_op = tf.group(validation_step.assign_add(1), ema.apply([top1_error, loss]),
                          ema2.apply([top1_error, loss]))
        top1_error_val = ema.average(top1_error)
        top1_error_avg = ema2.average(top1_error)
        loss_val = ema.average(loss)
        loss_val_avg = ema2.average(loss)

        # Summarize these values on tensorboard
        tf.summary.scalar('val_top1_error', top1_error_val)
        tf.summary.scalar('val_top1_error_avg', top1_error_avg)
        tf.summary.scalar('val_loss', loss_val)
        tf.summary.scalar('val_loss_avg', loss_val_avg)
        return val_op


    def full_validation(self, loss, top1_error, session, vali_data, vali_labels, batch_data,
                        batch_label):
        '''
        Runs validation on all the 10000 valdiation images
        :param loss: tensor with shape [1]
        :param top1_error: tensor with shape [1]
        :param session: the current tensorflow session
        :param vali_data: 4D numpy array
        :param vali_labels: 1D numpy array
        :param batch_data: 4D numpy array. training batch to feed dict and fetch the weights
        :param batch_label: 1D numpy array. training labels to feed the dict
        :return: float, float
        '''
        
        num_images = vali_data.shape[0]
        
        num_batches = num_images // FLAGS.validation_batch_size
        order = np.random.choice(num_images, num_batches * FLAGS.validation_batch_size)
        vali_data_subset = vali_data[order, ...]
        vali_labels_subset = vali_labels[order]

        loss_list = []
        error_list = []

        for step in range(num_batches):
            offset = step * FLAGS.validation_batch_size
            feed_dict = {self.image_placeholder: batch_data, self.label_placeholder: batch_label,
                self.vali_image_placeholder: vali_data_subset[offset:offset+FLAGS.validation_batch_size, ...],
                self.vali_label_placeholder: vali_labels_subset[offset:offset+FLAGS.validation_batch_size],
                self.lr_placeholder: FLAGS.init_lr,
                self.sweep_lambda_placeholder: self.sweep_lambda_value_validation,
                self.is_init_placeholder: self.is_init_value}
            loss_value, top1_error_value = session.run([loss, top1_error], feed_dict=feed_dict)
            loss_list.append(loss_value)
            error_list.append(top1_error_value)

        return np.mean(loss_list), np.mean(error_list)

log_path = 'C:/Users/shogun/Dropbox/MASTER/Master_Thesis/MLP/Experiments'
# maybe_download_and_extract()
# Initialize the Train object
    
    
experiment_type = FLAGS.profile_type if FLAGS.sweep_steps == 1 else "SWEEP"

sweep_on = "input" if FLAGS.apply_on_input == True else "output"
freeze = "freeze" if FLAGS.sweep_freeze == True else "NOfreeze"
log_file_name = '%s_on_%s_Steps_%s_%s_LR_%s_Batch_%s_Epochs_%s' % (experiment_type, sweep_on, str(FLAGS.sweep_steps), freeze, str(FLAGS.init_lr), str(FLAGS.train_batch_size), str(FLAGS.train_steps))

log_path = os.path.join(log_path, log_file_name)
print ("redirecting stdout to: " + log_path)
#sys.stdout = open(log_path + '.txt', 'w')

print("sweep_steps" + ": " + str(FLAGS.sweep_steps))
print("apply_on_input" + ": " + str(FLAGS.apply_on_input))
print("sweep_freeze" + ": " + str(FLAGS.sweep_freeze))
print("train_steps" + ": " + str(FLAGS.train_steps))
print("profile_type" + ": " + str(FLAGS.profile_type))

train = Train()

# Start the training session
train.train()
#train.validate_sweep_from_trained_model()

