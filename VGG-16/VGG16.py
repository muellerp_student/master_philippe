# Coder: Wenxin Xu
# Github: https://github.com/wenxinxu/resnet_in_tensorflow
# ==============================================================================
'''
This is the resnet structure
'''


import numpy as np
import math
from hyper_parameters import *
from cifar10_input import *

BN_EPSILON = 0.001

def create_variables(name, shape, initializer=tf.contrib.layers.xavier_initializer(), is_fc_layer=False, is_trainable = True):
    '''
    :param name: A string. The name of the new variable
    :param shape: A list of dimensions
    :param initializer: User Xavier as default.
    :param is_fc_layer: Want to create fc layer variable? May use different weight_decay for fc
    layers.
    :return: The created variable
    '''
    
    if is_fc_layer is True:
        regularizer = tf.contrib.layers.l2_regularizer(scale=FLAGS.weight_decay)
    else:
        regularizer = tf.contrib.layers.l2_regularizer(scale=FLAGS.weight_decay)

    new_variables = tf.get_variable(name, shape=shape, initializer=initializer,
                                    regularizer=regularizer, trainable=is_trainable)
    return new_variables


def linear_layer(input_layer, num_output_nodes, lambdas, is_IDP_layer, isTraining):
    '''
    :param input_layer: 2D tensor
    :param num_output_nodes: int. How many output labels in total? (10 for cifar10 and 100 for cifar100)
    :return: output layer Y = WX + B
    '''
    
    print("####### BUILDING LINEAR LAYER %d-CHANNELS #######" % num_output_nodes)

    assert len(input_layer.get_shape().as_list()) == 2

    input_dim = input_layer.get_shape().as_list()[-1]
    
    w = create_variables(name='weights', shape=[input_dim, num_output_nodes], is_fc_layer=True,
                            initializer=tf.uniform_unit_scaling_initializer(factor=1.0))
    b = create_variables(name='bias', shape=[num_output_nodes], initializer=tf.zeros_initializer())
    tf.add_to_collection(tf.global_variables, w)
    tf.add_to_collection(tf.global_variables, b)

    fc_h = tf.matmul(input_layer, w) + b
        
    tf.summary.histogram('fc_h' + '_activations', fc_h)
    tf.summary.scalar('fc_h' + '_sparsity', tf.nn.zero_fraction(fc_h))
        
    return fc_h

def convs_maxpool(input_batch, out_channels, nr_conv, lambdas, is_IDP_layer, isTraining, is_init, skip_maxpool):
    '''
    Defines a  block in VGG-16
    :param input_layer: 4D tensor (Batch, Height, Width, Depth)
    :param out_channels: int. return_tensor.get_shape().as_list()[-1] = output_channel
    :param nr_conv: as many conv layers are applied
    :return: 4D tensor, nr_conv times convolved and eventually once max-pooled with window size 2 and stride 2.
    '''
    
    assert len(input_batch.get_shape().as_list()) == 4
    input_dim = input_batch.get_shape().as_list()[-1]
    
    # param filter_shape: list. [filter_height, filter_width, filter_depth, filter_number]

    print("####### BUILDING BLOCK %d-CHANNELS #######" % out_channels)

    filter_shape = [3, 3, input_dim, out_channels]
    
    result = [input_batch]
    for n in range(nr_conv):
        print("conv nr: " + str(n))
        with tf.variable_scope('conv%d' % (n + 1 )):
            print("filter_shape: " + str(filter_shape))
            print("result[-1]: " + str(result[-1].get_shape()))
            conv = convolve(result[-1], filter_shape, lambdas, is_IDP_layer, isTraining, is_init)
            tf.summary.scalar('conv_layer_sparsity', tf.nn.zero_fraction(conv))
            relu_layer = tf.nn.relu(conv)
            tf.summary.scalar('relu_layer_sparsity', tf.nn.zero_fraction(relu_layer))
            
            result.append(relu_layer)
            filter_shape[2] = result[-1].get_shape().as_list()[-1]
    
    if skip_maxpool == False:
        max_pool = tf.nn.max_pool(result[-1], [1,2,2,1], [1,2,2,1], "VALID")
        tf.summary.scalar('max_pool_sparsity', tf.nn.zero_fraction(max_pool))    
        result.append(max_pool)
    
    return result[-1]

def batch_normalization_layer(input_layer, dimension):
    '''
    Helper function to do batch normalziation
    :param input_layer: 4D tensor
    :param dimension: input_layer.get_shape().as_list()[-1]. The depth of the 4D tensor
    :return: the 4D tensor after being normalized
    '''
    mean, variance = tf.nn.moments(input_layer, axes=[0, 1, 2])

    beta = tf.get_variable('beta', dimension, tf.float32,
                               initializer=tf.constant_initializer(0.0, tf.float32))
    gamma = tf.get_variable('gamma', dimension, tf.float32,
                                initializer=tf.constant_initializer(1.0, tf.float32))
    tf.add_to_collection(tf.global_variables, beta)    
    tf.add_to_collection(tf.global_variables, gamma)
    bn_layer = tf.nn.batch_normalization(input_layer, mean, variance, beta, gamma, BN_EPSILON)
    return bn_layer
    
def convolve(input_layer, filter_shape, lambdas, is_IDP_layer, isTraining, is_init):
    
    out_channels = filter_shape[-1]

    in_channels = filter_shape[-2]
    if FLAGS.use_bn == True:
        input_layer = batch_normalization_layer(input_layer, in_channels)

    
    if FLAGS.sweep_freeze == True and FLAGS.sweep_steps > 1.0 and is_IDP_layer == True:
        filters = []
        print("***  FREEZABLE VARIABLES CREATION ***")        
        # create an own filter for each output channel, to make freezing possible
        split_filter_shape = filter_shape.copy()
        split_filter_shape[-1] = 1
        for n in range(out_channels):
            temp_filter = create_variables(name='weights_%d' % n, shape=split_filter_shape)
            filters.append(temp_filter)
            if isTraining == True:
                tf.add_to_collection("IDP_CHANNEL_WEIGHTS", temp_filter)
                tf.add_to_collection(tf.global_variables, temp_filter)      
        print("filters list: " + str(filters))
    else:
        print("***  NORMAL VARIABLES CREATION ***")                
        big_filter = create_variables(name='big_filter', shape=filter_shape)
        tf.add_to_collection(tf.global_variables, big_filter)

    if FLAGS.profile_type == "harmonic":
        profile = tf.Variable(np.fromfunction(lambda i: 1/(i+1), (in_channels, )), trainable=False, dtype=tf.float32, name ="harmonic_coefficients")
    elif FLAGS.profile_type == "linear":
        profile = tf.Variable(np.fromfunction(lambda i: 1 - i/in_channels, (in_channels, )), trainable=False, dtype=tf.float32, name ="linear_coefficients")
    elif FLAGS.profile_type == "all-ones":
        profile = tf.Variable(np.fromfunction(lambda i: 1, (in_channels, )), trainable=False, dtype=tf.float32)
    else:
        profile = tf.Variable(np.fromfunction(lambda i: 1, (in_channels, )), trainable=False, dtype=tf.float32)
    
    tf.summary.histogram('profile', profile)
    
    if is_IDP_layer == True:
            print("########## IDP Layer ##########  ")
            if FLAGS.apply_on_input == True:
                print("***  INPUT BRANCH TAKEN ***")
                # this branch is for both only profile and only sweep. 
                # for profiles, the sweep value is always one (except at the very end when checking the result)
                # for sweep on input, the profile is just an all-ones
                # use profile on input channels
                # also apply the sweep at the end of training
                h = lambdas*in_channels
                print("h.get_shape()" + str(h.get_shape()))
                
#                for i in range(2):
#                    h = tf.expand_dims(h,i+2)
                c = tf.range(in_channels, dtype=tf.float32) # (C_in)
                h = tf.subtract(h, c)
                print("h.get_shape() subtracted" + str(h.get_shape()))

                h = tf.maximum(tf.minimum(h,1),0)
                print("h.get_shape() maxmin" + str(h.get_shape()))
                print("len(h.get_shape()) maxmin" + str(len(h.get_shape())))
                h = tf.expand_dims(h, len(h.get_shape()))
                profile = tf.expand_dims(profile, len(profile.get_shape()))
                print("h.get_shape() final" + str(h.get_shape()))                
                tf.summary.histogram(h.name, h)
                
                sweeped_filter = tf.multiply(big_filter, h, name = "apply_sweep")
                profiled_filter = tf.multiply(sweeped_filter, profile, name="apply_profile")
                tf.summary.scalar('profiled_filter_sparsity', tf.nn.zero_fraction(profiled_filter))
                tf.summary.scalar('is_init_value', tf.cast(is_init, tf.int8))
                active_filter = tf.cond(is_init, lambda: big_filter, lambda: profiled_filter)
                return tf.nn.conv2d(input_layer, active_filter, strides=[1, 1, 1, 1], padding='SAME')
            else:
                print("***  OUTPUT BRANCH TAKEN ***")                
                h = lambdas*out_channels
#                for i in range(2):
#                    h = tf.expand_dims(h,i+2)
                c = tf.range(out_channels, dtype=tf.float32) # (C_out)
                h = tf.subtract(h, c)
                print("h.get_shape() subtracted" + str(h.get_shape()))                
                h = tf.maximum(tf.minimum(h,1),0)
                print("h.get_shape() maxmin" + str(h.get_shape()))
                
                if FLAGS.sweep_freeze == False:
                    print("*** JOINT BRANCH TAKEN ***")                    
                    # just set the corresponding output nodes to zero. Inital idea of sweep to save computation by means of pruning channels.
                    # this means joint training of more and more weights
                    conv_layer = tf.nn.conv2d(input_layer, big_filter, strides=[1, 1, 1, 1], padding='SAME')
                    return tf.cond(is_init, lambda: conv_layer, lambda: tf.multiply(conv_layer, h))
                else:
                    print("*** FREEZE BRANCH TAKEN ***")                                        
                    # freeze some weights by giving a var_list to the optimizer such that only the current active sweep area is updated.
                    # to set the remaining output channels to zero, the filter is multiplied with the corresponding h
                    out_list = []
                    for n in range(out_channels):
#                        print("layer : " + h.name)
#                        print("n : " + str(n))
#                        print("h.get_shape()" + str(h.get_shape()))
#                        print("h[n].get_shape()" + str(h[n].get_shape()))
                        tf.summary.scalar('h%d_' % n + '/sparsity', tf.nn.zero_fraction(h[n]))
                        active_filter = tf.cond(is_init, lambda: filters[n], lambda: tf.multiply(filters[n],h[n]))
                        conv = tf.nn.conv2d(input_layer, active_filter, strides=[1, 1, 1, 1], padding='SAME')
                        print("conv result appending, shape: : " + str(conv.get_shape()))
                        out_list.append(conv)
                        
                    print("out_list" + str(out_list))
                    return tf.concat(out_list, axis = 3)
    else:
        return tf.nn.conv2d(input_layer, big_filter, strides=[1, 1, 1, 1], padding='SAME')


def inference(input_tensor_batch, external_lambda, is_init, reuse):
    '''
    The main function that defines the ResNet. total layers = 1 + 2n + 2n + 2n +1 = 6n + 2
    :param input_tensor_batch: 4D tensor
    :param n: num_residual_blocks
    :param reuse: To build train graph, reuse=False. To build validation graph and share weights
    with train graph, resue=True
    :return: last layer in the network. Not softmax-ed
    '''
    tf.summary.scalar('external_lambda in inference', external_lambda)

    in_shape = input_tensor_batch.get_shape().as_list()
    print ("input_tensor_batch" + str(input_tensor_batch.get_shape()))
    batch_size = in_shape[0]
    print ("batch_size" + str(batch_size))

    input_dim = in_shape[1]
    print ("input_dim" + str(input_dim))

    
    assert input_tensor_batch.get_shape().as_list() == [FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size,  IMG_HEIGHT, IMG_WIDTH, IMG_DEPTH]
    
    layers = []
    with tf.variable_scope('init_conv64', reuse=reuse):
        filter = create_variables(name='conv', shape=(3, 3, IMG_DEPTH, 64))
        conv_layer = tf.nn.conv2d(input_tensor_batch, filter, strides=[1, 1, 1, 1], padding='SAME')
        tf.summary.scalar('init_conv64', tf.nn.zero_fraction(conv_layer))        
        layers.append(conv_layer)
    assert layers[-1].get_shape().as_list() == [FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size,  IMG_HEIGHT, IMG_WIDTH, 64]

    with tf.variable_scope('IDP__64', reuse=reuse):
        layers.append(convs_maxpool(layers[-1], 64, 2, external_lambda, True, not reuse, is_init, False))
    assert layers[-1].get_shape().as_list() == [FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size,  IMG_HEIGHT // 2, IMG_WIDTH // 2, 64]

    with tf.variable_scope('IDP__128', reuse=reuse):
        layers.append(convs_maxpool(layers[-1], 128, 2, external_lambda, True, not reuse, is_init, False))
        
    assert layers[-1].get_shape().as_list() == [FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size,  IMG_HEIGHT // 2**2, IMG_WIDTH // 2**2, 128]
        
        
    with tf.variable_scope('IDP__256', reuse=reuse):
        layers.append(convs_maxpool(layers[-1], 256, 3, external_lambda, True, not reuse, is_init, False))
        
    assert layers[-1].get_shape().as_list() == [FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size,  IMG_HEIGHT // 2**3, IMG_WIDTH // 2**3, 256]
        
    with tf.variable_scope('IDP__512', reuse=reuse):
        layers.append(convs_maxpool(layers[-1], 512, 3, external_lambda, True, not reuse, is_init, True))
        
    assert layers[-1].get_shape().as_list() == [FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size,  IMG_HEIGHT // 2**3, IMG_WIDTH // 2**3, 512]

    with tf.variable_scope('IDP_again_512', reuse=reuse):
        layers.append(convs_maxpool(layers[-1], 512, 3, external_lambda, True, not reuse, is_init, False))
        
    assert layers[-1].get_shape().as_list() == [FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size,  IMG_HEIGHT // 2**4, IMG_WIDTH // 2**4, 512]
        
    with tf.variable_scope('FC_512', reuse=reuse):
        flat_input = tf.reshape(layers[-1], (FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size, -1))
        linear = linear_layer(flat_input, 512, external_lambda, False, not reuse)
        layers.append(linear)

    assert layers[-1].get_shape().as_list() == [FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size,  512]
        
    with tf.variable_scope('FC_10', reuse=reuse):
        linear = linear_layer(layers[-1], 10, external_lambda, False, not reuse)
        layers.append(linear)
    
    assert layers[-1].get_shape().as_list() == [FLAGS.validation_batch_size if reuse else FLAGS.train_batch_size,  10]
        
    return layers[-1]
