#Submit to cluster to gpu
FILE=$1
TAG=$2
DATE=$(date +%d%m%Y_%H%M%S)
NAME=${TAG}_${DATE}_${FILE}
TRAIN_HOME_DIR=/srv/glusterfs/muellerp/variant_resnet/${TAG}

DATA_DIR=/srv/glusterfs/muellerp/cifar10_data/
TRAIN_DIR=/srv/glusterfs/muellerp/variant_resnet/${TAG}/${DATE}

IS_FULL_VALIDATION=True
INIT_LR=$3
LR_DECAY_FACTOR=$4
SWEEP_STEPS=$5
SWEEP_TO=$6
SWEEP_FREEZE=$7
APPLY_ON_INPUT=$8
PROFILE_TYPE=$9
TRAIN_STEPS=${10}
TRAIN_BATCH_SIZE=${11}
VAL_BATCH_SIZE=${12}
INIT_PERCENT=${13}
USE_BN=${14}
export NAME

if [ $# -lt 14 ]
  then
    echo "Please provide 1. file name 2. tag 3. INIT_LR 4. LR_DECAY_FACTOR 5. SWEEP_STEPS 6. SWEEP_TO 7. SWEEP_FREEZE 8. APPLY_ON_INPUT 9. PROFILE_TYPE 10. TRAIN_STEPS 11. TRAIN_BATCH_SIZE 12. VAL_BATCH_SIZE 13. INIT_PERCENT 14. USE_BN"
else

mkdir ${TRAIN_HOME_DIR}
mkdir ${TRAIN_DIR}
mkdir ${TRAIN_DIR}/source
cp *.py ${TRAIN_DIR}/source 


echo "submitting job to cluster..."
  qsub -N $NAME -l gpu ./pywrap_gpu.sh ${TRAIN_DIR}/source/cifar10_train.py --train_steps $TRAIN_STEPS --data_dir $DATA_DIR --train_dir $TRAIN_DIR --tag $TAG --is_full_validation $IS_FULL_VALIDATION --init_lr $INIT_LR --lr_decay_factor $LR_DECAY_FACTOR --sweep_steps $SWEEP_STEPS --sweep_to $SWEEP_TO --sweep_freeze $SWEEP_FREEZE --apply_on_input $APPLY_ON_INPUT --profile_type $PROFILE_TYPE --train_steps $TRAIN_STEPS --train_batch_size $TRAIN_BATCH_SIZE --validation_batch_size $VAL_BATCH_SIZE --init_percent $INIT_PERCENT --use_bn $USE_BN
fi
