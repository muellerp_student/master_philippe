from resnet import * 
from random import *
import skimage.io  # bug. need to import this before tensorflow
import skimage.transform  # bug. need to import this before tensorflow
import tensorflow as tf
import os
import sys 

MOMENTUM = 0.9
def ensure_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return
        
def printMaskWithWeights(sess, FLAGS):
    with sess.graph.as_default():
        print("Here are some filters and their masks,\n thresholded by: " + str(FLAGS.threshold_weights))

        count = 3
        for v in tf.trainable_variables():
            rand_pick = random()
            if 'weights' not in v.name or 'Momentum' in v.name or 'block' not in v.name or rand_pick < 0.4:
                continue
            if count < 1:
                break
            value = v.eval(session=sess)
            rand_dim_1 = randint(1, value.shape[2]-1)
            rand_dim_2 = randint(1, value.shape[3]-1)
            for m in tf.global_variables():
                if 'mask' not in m.name or 'Momentum' in m.name:
                    continue
                if os.path.split(v.name)[0] + '/mask_scope' == os.path.split(m.name)[0]:
                    mask = m.eval(session=sess)
                    print(m.name + " looks like:")
                    print(str(mask[:,:,rand_dim_1,rand_dim_2]))
                    print("from weights " + v.name)
                    print(str(value[:,:,rand_dim_1,rand_dim_2]))
            count -= 1

def top_k_error(predictions, labels, k, batch_size):
    batch_size = float(batch_size) 
    in_top1 = tf.to_float(tf.nn.in_top_k(predictions, labels, k))
    num_correct = tf.reduce_sum(in_top1)
    return (batch_size - num_correct) / batch_size

def nr_correct_k(predictions, labels, k, batch_size):
    batch_size = float(batch_size) 
    in_top1 = tf.to_float(tf.nn.in_top_k(predictions, labels, k))
    num_correct = tf.reduce_sum(in_top1)
    return num_correct

def train(is_training, logits, images, labels, isFineTune, FLAGS, restore_path = None):
    if isFineTune:
        print("%%%%%%%%%% Starting FineTuning %%%%%%%%%%")
    else:
        print("%%%%%%%%%% Starting BaseLine Training %%%%%%%%%%")
        
    global_step = tf.get_variable('global_step', [],
                              initializer=tf.constant_initializer(0),
                              trainable=False)
    val_step = tf.get_variable('val_step', [],
                              initializer=tf.constant_initializer(0),
                              trainable=False)
    
    separator = '/' if FLAGS.max_steps == 64000 else '\\'
    
    reset_steps_op = tf.group(tf.assign(global_step, 0), tf.assign(val_step, 0))
    loss_ = loss(logits, labels)
    predictions = tf.nn.softmax(logits)

    top1_error = top_k_error(predictions, labels, 1, FLAGS.batch_size)
    top5_error = top_k_error(predictions, labels, 5, FLAGS.batch_size)

    # loss_avg
    ema = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY, global_step)
    tf.add_to_collection(UPDATE_OPS_COLLECTION, ema.apply([loss_]))
    tf.summary.scalar('loss_avg', ema.average(loss_))

    # validation stats
    ema = tf.train.ExponentialMovingAverage(0.9, val_step)
    
    val_op = tf.group(val_step.assign_add(1), ema.apply([top5_error, top1_error]))

    top1_error_avg = ema.average(top1_error)
    top5_error_avg = ema.average(top5_error)
    
    tf.summary.scalar('val_top1_error_avg', top1_error_avg)
    tf.summary.scalar('val_top5_error_avg', top5_error_avg)
    tf.summary.scalar('learning_rate', FLAGS.learning_rate)
    
    learning_rate_placeholder = tf.placeholder(tf.float32, [], name='learning_rate')    
    opt = tf.train.MomentumOptimizer(learning_rate_placeholder, MOMENTUM)
    
    print("setting up gradient operations..")
    grads = opt.compute_gradients(loss_)

    for grad, var in grads:
        if grad is not None and not FLAGS.minimal_summaries:
            tf.summary.histogram(var.op.name + '/gradients', grad)

    
    apply_gradient_op = opt.apply_gradients(grads, global_step=global_step)

    if not FLAGS.minimal_summaries:
        # Display the training images in the visualizer.
        tf.summary.image('images', images)

        for var in tf.trainable_variables():
            tf.summary.histogram(var.op.name, var)
                
                
    batchnorm_updates = tf.get_collection(UPDATE_OPS_COLLECTION)
    batchnorm_updates_op = tf.group(*batchnorm_updates)
    train_op = tf.group(apply_gradient_op, batchnorm_updates_op)

    saver = tf.train.Saver(tf.global_variables())

    summary_op = tf.summary.merge_all()
    
    init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # Do not assign whole gpu memory, just use it on the go
    config.allow_soft_placement = True  # If a operation is not define it the default device, let it execute in another.
    sess = tf.Session(config=config)
    sess.run(init)
    
    tf.train.start_queue_runners(sess=sess)

    summary_writer = tf.summary.FileWriter(FLAGS.train_dir, sess.graph)

    print('restore_path :' + restore_path)
    if not restore_path == None:
        latest = tf.train.latest_checkpoint(restore_path)
        if not latest:
            print("No checkpoint to continue from in ", restore_path)
            sys.exit(1)
        print("Model " + restore_path + " restored.")
        saver.restore(sess, latest)
        sess.run(reset_steps_op)
        stage_dir = os.path.normpath(restore_path).split(separator)[-1]
        print("stage_dir :" + stage_dir)
        if not stage_dir == 'pruned':
            parent_dir = os.path.normpath(restore_path).split(separator)[-2]
            print("parent_dir :" + parent_dir)
        
    print("%%%%%%%%%%%% Before Training Loop %%%%%%%%%%%%")
#    uncomment to print some masks and corresponding values 
#    printMaskWithWeights(sess,FLAGS)
    
    iterations = FLAGS.max_steps if not isFineTune else FLAGS.finetune_steps
    for x in range(iterations + 1):
        start_time = time.time()

        step = sess.run(global_step)
        learning_rate = FLAGS.learning_rate if step < 32000 else (FLAGS.learning_rate / 10 if step < 48000 else FLAGS.learning_rate / 100)
        learning_rate = FLAGS.learning_rate_finetune if isFineTune else learning_rate
        i = [train_op, loss_]

        write_summary = step % 100 and step > 1
        if write_summary:
            i.append(summary_op)

        o = sess.run(i, feed_dict={is_training: True, learning_rate_placeholder: learning_rate})
           
        loss_value = o[1]

        duration = time.time() - start_time

        assert not np.isnan(loss_value), 'Model diverged with loss = NaN'

        if step % 500 == 0:
            examples_per_sec = FLAGS.batch_size / float(duration)
            format_str = ('step %d, loss = %.2f (%.1f examples/sec; %.3f sec/batch)')
            print(format_str % (step, loss_value, examples_per_sec, duration))

        if write_summary:
            summary_str = o[2]
            summary_writer.add_summary(summary_str, step)

        # Save the model checkpoint periodically.
        if step > 1 and step % 1000 == 0:
            if restore_path == None:
                checkpoint_path = os.path.join(FLAGS.models_dir, 'base/model_base.ckpt')
            elif stage_dir == 'pruned':
                checkpoint_path = os.path.join(FLAGS.models_dir, 'pruned/model_pruned.ckpt')
            elif parent_dir == 'channels':
                checkpoint_path = os.path.join(FLAGS.models_dir, parent_dir, stage_dir, 'model.ckpt')                
            saver.save(sess, checkpoint_path, global_step=global_step)

        # Run validation periodically
        if step > 1 and step % 1000 == 0:
            _, top1_error_value, top5_error_value = sess.run([val_op, top1_error, top5_error], { is_training: False })
            print('Validation top1 error %.4f' % top1_error_value)
            print('Validation top5 error %.4f' % top5_error_value)
    
    
    saver_final = tf.train.Saver(tf.global_variables())
    if restore_path == None:
        final_models_path = os.path.join(FLAGS.models_dir, 'final_models', 'base')  
    elif stage_dir == 'pruned':
        final_models_path = os.path.join(FLAGS.models_dir,'final_models', 'pruned')
    elif parent_dir == 'channels':
        final_models_path = os.path.join(FLAGS.models_dir,'final_models', parent_dir, stage_dir)
        ensure_dir(final_models_path)
                
    model_path = os.path.join(final_models_path, 'final.ckpt') 
    saver_final.save(sess, model_path, global_step=global_step)

    return

def evaluate(logits, labels, model_path, nr_batches, FLAGS, is_training, top1, top5):
    global_step = tf.get_variable('global_step', [],
                              initializer=tf.constant_initializer(0),
                              trainable=False)
    val_step = tf.get_variable('val_step', [],
                              initializer=tf.constant_initializer(0),
                              trainable=False)
    reset_steps_op = tf.group(tf.assign(global_step, 0), tf.assign(val_step, 0))
    reset_top_op = tf.group(tf.assign(top1, 0), tf.assign(top5, 0))
    reset_measures_op = tf.group(reset_steps_op, reset_top_op)
    
    predictions = tf.nn.softmax(logits)
    
    batch_nr_correct_1 = nr_correct_k(predictions, labels, 1, FLAGS.batch_size)
    batch_nr_correct_5 = nr_correct_k(predictions, labels, 5, FLAGS.batch_size)
    
    val_op = tf.group(val_step.assign_add(1), batch_nr_correct_1, batch_nr_correct_5)
    
#    tf.summary.scalar('val_top1_error_avg', top1_error_avg)
#    tf.summary.scalar('val_top5_error_avg', top5_error_avg)

    for var in tf.trainable_variables():
        tf.summary.histogram(var.op.name, var)

    
    list_to_restore = []
    for item in tf.global_variables():
        if item.name != "top1:0" and item.name != "top5:0":
            list_to_restore.append(item)

    saver = tf.train.Saver(list_to_restore)

    summary_op = tf.summary.merge_all()
    init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # Do not assign whole gpu memory, just use it on the go
    config.allow_soft_placement = True  # If a operation is not define it the default device, let it execute in another.
    sess = tf.Session(config=config)
    
    sess.run(init)
    sess.run(reset_measures_op)
        
    tf.train.start_queue_runners(sess=sess)

    latest = tf.train.latest_checkpoint(model_path)
    if not latest:
        print("No checkpoint to continue from in", model_path)
        sys.exit(1)
    saver.restore(sess, latest)
    
#    uncomment to print some masks and corresponding values     
#    printMaskWithWeights(sess,FLAGS)
    for x in range(nr_batches):
        step = sess.run(global_step)
        
        _, batch_nr_correct_1_value, batch_nr_correct_5_value = sess.run([val_op, batch_nr_correct_1, batch_nr_correct_5], { is_training: False })
        update_top = tf.group(top1.assign_add(batch_nr_correct_1_value), top5.assign_add(batch_nr_correct_5_value))
        sess.run(update_top)
#        print("batch_nr_correct_1_value: " + str(batch_nr_correct_1_value))
#        print("batch_nr_correct_5_value: " + str(batch_nr_correct_5_value))
        
    correct_1 = top1.eval(session=sess)
    correct_5 = top5.eval(session=sess)
    total_processed = nr_batches * 16
    total_top_1 =  (total_processed - correct_1) / total_processed
    total_top_5 =  (total_processed - correct_5) / total_processed
    print("total processed: " + str(total_processed))
    print("correct_1: " + str(correct_1))
    print("correct_5: " + str(correct_5))
    print("---> top1 error: {:2.2f} % \n".format(total_top_1*100))
    print("---> top5 error: {:2.2f} % \n".format(total_top_5*100))
    
    return total_top_1, total_top_5
