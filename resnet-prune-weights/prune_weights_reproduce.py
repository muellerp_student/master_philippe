# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 14:11:58 2017

@author: shogun
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys
import tarfile

from six.moves import xrange  # pylint: disable=redefined-builtin
from six.moves import urllib

import argparse
import numpy as np

import resnet_train
import resnet
import train_cifar
import tensorflow as tf

FLAGS = None

def ensure_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return
        
def GetCheckpoint(path):
    latest = tf.train.latest_checkpoint(path)
    if not latest:
        print("No checkpoint to continue from in " + path)
        sys.exit(1)
    print("Model " + path + " restored.")

    return latest

def GetNumberOfWeights(shape):
    res = 1.0
    for dim in shape:
        res *= dim.value
    return res

def main(_):  # pylint: disable=unused-argument

    base_train_path = os.path.join(FLAGS.models_dir, 'base/')
    channel_path = os.path.join(FLAGS.models_dir, 'channels/')
    final_models_path = os.path.join(FLAGS.models_dir, 'final_models/')
    final_base_path = os.path.join(final_models_path, 'base/')
    final_channel_path = os.path.join(final_models_path, 'channels/')
    
    do_baseline_training = False
    do_weight_pruning = False
    do_channel_l2 = False
    do_eval_of_all = True
    
    if do_baseline_training:
        print("%%%%%%%%%%%% Step 1 %%%%%%%%%%%%%%%")
        print("%%%%%%% Baseline Training %%%%%%%%%")
        print("Solver Configuration: ")
        print("Iterations : " + str(FLAGS.max_steps))        
        print("Base Learning Rate : " + str(FLAGS.learning_rate))
        print("Batch Size : " + str(FLAGS.batch_size))    
        print("Batch Size : " + str(FLAGS.batch_size))    
        train_cifar.start(FLAGS, False, None)

    if do_weight_pruning:
        print("%%%%%%%%%%% Step 2 %%%%%%%%%%%%%%%")
        print("%%%%%%% Weight - Pruning %%%%%%%%%")
    
        saver = tf.train.import_meta_graph(final_base_path + 'final_model_base.ckpt-64001.meta')
        sess = tf.Session()
        
        latest = GetCheckpoint(base_train_path)
            
        #restore initializes all variables
        saver.restore(sess,latest)
        
        print("%%% Tensor Masks %%%")
        total_weights_fc = 0
        total_blocked_fc = 0
        total_weights_conv = 0
        total_blocked_conv = 0
        for v in tf.global_variables():
            if 'weights' not in v.name or 'Momentum' in v.name:
                continue
            
            value = v.eval(session=sess)
            
            for m in tf.global_variables():
                
                if 'mask' not in m.name or 'Momentum' in m.name:
                    continue
                if os.path.split(v.name)[0] + '/mask_scope' == os.path.split(m.name)[0]:
                    mask = (abs(value) > FLAGS.threshold_weights).astype(float)
    
                    assign_op = tf.assign(m, mask)
                    sess.run(assign_op)
                    
                    if "fc/weights" in v.name:
                        print("Fully Connected Mask " + m.name)
                        nr_zeros_fc = (mask.size - np.sum(mask)).astype(int)
                        nr_weights_fc = GetNumberOfWeights(v.get_shape())
                        total_weights_fc += nr_weights_fc
                        total_blocked_fc += nr_zeros_fc
                        print("blocks {} of {} elements".format(nr_zeros_fc, nr_weights_fc) + "\n")
    
                    else:
                        
                        print("Mask " + m.name)
                        nr_zeros_conv = (mask.size - np.sum(mask)).astype(int)
                        nr_weights_conv = GetNumberOfWeights(v.get_shape())
                        total_weights_conv += nr_weights_conv
                        total_blocked_conv += nr_zeros_conv
                        print("blocks {} of {} elements".format(nr_zeros_conv, nr_weights_conv) + "\n")
                    break
                
        print("%%%% Mask Summary:")
        print("Conv:")
        print("{} weights out of {} are blocked:".format(total_blocked_conv, total_weights_conv))
        print("--->  {:2.2f} % blocked \n".format(total_blocked_conv/total_weights_conv*100))
        print("FC:")
        print("{} weights out of {} are blocked:".format(total_blocked_fc, total_weights_fc))
        print("--->  {:2.2f} % blocked \n".format(total_blocked_fc/total_weights_fc*100))
        
        ### Saving the freshly masked but still unusable pruned model in the normal pruned folder
        # The final model will be saved in the finals folder
        
        saver_pruned = tf.train.Saver(tf.global_variables())
        pruned_path = os.path.join(FLAGS.models_dir, 'pruned/')
        model_pruned_path = os.path.join(pruned_path, 'model_pruned.ckpt')
        saver_pruned.save(sess, model_pruned_path)
        print("Saved pruned model in: " + str(model_pruned_path))
        
        tf.reset_default_graph()
    
        # Step 3: Fine-Tune the net
        print("%%%%%% Step 3 %%%%%%%%%%")
        print("%%%%%% fine tune net %%%%%%%%%%")
    
        train_cifar.start(FLAGS, True, pruned_path)
        print("%%%%%% Load variables from last checkpoint %%%%%%%%%%")
        
        # Step 4: Compare performance of the pruned and the unpruned net (final versions)
        print("%%%%%% Step 4 %%%%%%%%%%")
        print("%%%% Evaluate Base Model%%%%%%")
        tf.reset_default_graph()
        base_model_path = os.path.join(final_models_path, 'base/') 
        train_cifar.validation(FLAGS, base_model_path)
        
        print("%%%% Evaluate Pruned Model%%%%%%")
        tf.reset_default_graph()
        pruned_model_path = os.path.join(final_models_path, 'pruned/') 
        
        train_cifar.validation(FLAGS, pruned_model_path)


    
    if do_channel_l2:        
        print("%%% Step 5: Prune channels by l2 of the output-channel-weights %%%")
        for exp in range(1, 5):
            tf.reset_default_graph()
        
            saver = tf.train.import_meta_graph(final_base_path + 'final_model_base.ckpt-64001.meta')
            sess = tf.Session()
            latest = GetCheckpoint(final_base_path)
            saver.restore(sess,latest)
            
            print("%%% Channels %%%")
            total_weights_fc = 0
            total_blocked_fc = 0
            total_weights_conv = 0
            total_blocked_conv = 0
            for v in tf.global_variables():
                if 'weights' not in v.name or 'Momentum' in v.name:
                    continue
                if 'A' not in v.name:
                    continue
                
                value = v.eval(session=sess)
                rel_channel_to_prune = 1 / 2**exp
                for m in tf.global_variables():
                    if 'mask' not in m.name or 'Momentum' in m.name:
                        continue
                    if os.path.split(v.name)[0] + '/mask_scope' == os.path.split(m.name)[0]:
                        print('value.shape :' + str(value.shape))
                        nr_channels_to_prune = int(rel_channel_to_prune * value.shape[3])
        # uncomment to compute norm another way, numerical differences!
        #                channel_norm = np.ones((value.shape[3]))
        #                for outchannel in range(value.shape[3]):
        #                    vector = np.copy(value[:,:,:,outchannel])
        #                    vector = np.resize(vector,(1,-1))
        #                    channel_norm[outchannel] = np.linalg.norm(vector)
        #                    print(v.name)
        ##                    print('value[:,:,:,outchannel] :' + str(value[:,:,:,outchannel]))
        #                    print('channel_norm :' + str(channel_norm))
                        
                        l2_norm = np.linalg.norm(np.linalg.norm(np.linalg.norm(value, axis=2), axis=1), axis=0)
                        print('removing outputchannel ' + str(l2_norm.argsort()[:nr_channels_to_prune]) + ' from tensor ' + str(v.name) + ' with shape ' + str(value.shape))
                        mask = np.ones(value.shape).astype(float)
                        minimum_norm_indexes = np.array(l2_norm.argsort()[:nr_channels_to_prune])
                        mask[:,:,:,minimum_norm_indexes] = 0.0
                        
        # uncomment to ensure the mask is zero where required
        #                for dim in range(value.shape[2]):
        #                    print("mask[:,:,dim,minimum_norm_indexes] \n" + str(np.reshape(mask[:,:,dim,minimum_norm_indexes], (3,3))))
                            
                        assign_op = tf.assign(m, mask)
                        sess.run(assign_op)
                        
                        if "fc/weights" in v.name:
                            print("Fully Connected Mask " + m.name)
                            nr_zeros_fc = (mask.size - np.sum(mask)).astype(int)
                            nr_weights_fc = GetNumberOfWeights(v.get_shape())
                            total_weights_fc += nr_weights_fc
                            total_blocked_fc += nr_zeros_fc
                            print("blocks {} of {} elements".format(nr_zeros_fc, nr_weights_fc) + "\n")
        
                        else:
                            print("Mask " + m.name)
                            nr_zeros_conv = (mask.size - np.sum(mask)).astype(int)
                            nr_weights_conv = GetNumberOfWeights(v.get_shape())
                            total_weights_conv += nr_weights_conv
                            total_blocked_conv += nr_zeros_conv
                            print("blocks {} of {} elements".format(nr_zeros_conv, nr_weights_conv) + "\n")
                        break
                    
            print("%%%% Mask Summary:")
            print("Conv:")
            print("{} weights out of {} are blocked:".format(total_blocked_conv, total_weights_conv))
            print("--->  {:2.2f} % blocked \n".format(total_blocked_conv/total_weights_conv*100))
            
            saver_channel = tf.train.Saver(tf.global_variables())
            channel_model_path = 'dir_{:1.1f}%_channels'.format(rel_channel_to_prune*100)
            dir_path = os.path.join(channel_path, channel_model_path)
            ensure_dir(dir_path)
            model_name = 'model.ckpt'
            saver_channel.save(sess, os.path.join(dir_path , model_name))
            print("Saved channel model in: " + str(dir_path))
            
            tf.reset_default_graph()
            train_cifar.start(FLAGS, True, dir_path)

    
    if do_eval_of_all:
        print("%%%%%% Step 6: Compare performance of all nets %%%%%%%%%%")
        
        result_dict = {}
        rootDir = final_models_path
        for dirName, subdirList, files in os.walk(rootDir):
            if files:
                tf.reset_default_graph()
                result_dict[dirName] = train_cifar.validation(FLAGS, dirName)
        print("Results: ")
        for k in result_dict:
            print("{} :\n top_1 : {:2.1f} \n top_5 : {:2.1f} \n".format(k, result_dict[k][0]*100, result_dict[k][1]*100))    

#if __name__ == '__main__':
if True:
  parser = argparse.ArgumentParser()
  
  parser.add_argument(
      '--max_steps',
      type=int,
      default=20,
      help='Number of steps'
  )
  parser.add_argument(
      '--finetune_steps',
      type=int,
      default=20,
      help='Number of steps'
  )
  parser.add_argument(
      '--batch_size',
      type=int,
      default=128,
      help='Batch size.'
  )
  parser.add_argument(
      '--learning_rate',
      type=float,
      default=0.1,
      help='learning rate'
  )
  parser.add_argument(
      '--learning_rate_finetune',
      type=float,
      default=0.01,
      help='finetune learning rate'
  )
  parser.add_argument(
      '--train_dir',
      type=str,
      default='/tmp/resnet_train',
      help='Directory where to write event logs and checkpoints.'
  )  
  parser.add_argument(
      '--models_dir',
      type=str,
      default='/tmp/models_dir',
      help='Directory to save the models to compare'
  )
  parser.add_argument(
      '--data_dir',
      type=str,
      default='/tmp/cifar-data',
      help='Directory holding the data'
  )
  parser.add_argument(
      '--resume',
      type=bool,
      default=False,
      help='resume from latest saved state'
  )
  parser.add_argument(
      '--minimal_summaries',
      type=bool,
      default=True,
      help='produce fewer summaries to save HD space'
  )
  parser.add_argument(
      '--use_bn',
      type=bool,
      default=True,
      help='use batch normalization if true, else use biases'
  )
  parser.add_argument(
      '--threshold_weights',
      type=float,
      default='0.05',
      help='threshold for weights'
  )
  parser.add_argument(
      '--threshold_channels',
      type=float,
      default='2.1',
      help='threshold for weights'
  )
  
  FLAGS, unparsed = parser.parse_known_args()
#  print("FLAGS:")
#  for k in FLAGS.__dict__:
#      print("{} : {}".format(k, FLAGS.__dict__[k]))
#  print("\n")
  
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)

