#!/bin/bash
#$ -l h_rt=8:00:00
#$ -l h_vmem=20G
#$ -l h="biwirender0[5-9]|biwirender[1-9][0-9]"
#$ -cwd
#$ -V
#$ -j y
#$ -o ../logs/output
#Usage: ./pywrap.sh filename.py --lr 0.000001 etc
echo "Arguments given to pywrap.sh :"
echo "$@"
source ~/glusterfs/gpu_tensorflow_35/bin/activate
export CUDA_VISIBLE_DEVICES=${SGE_GPU}
echo "CUDA_VISIBLE_DEVICES in pywrap.sh: $CUDA_VISIBLE_DEVICES"
#BRANCH=$1
#shift 1
#BASE=/scratch/aeirikur/hashnets_experiments
#mkdir -p ${BASE}
#TMPDIR=$(mktemp -d -p ${BASE} )
#cd ${TMPDIR}
#git clone -b ${BRANCH} ~/projects/hashnets/ .
python "$@"
