# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 14:11:58 2017

@author: shogun
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys
import tarfile

from six.moves import xrange  # pylint: disable=redefined-builtin
from six.moves import urllib

import argparse
import numpy as np

import resnet_train
import resnet
import train_cifar
import tensorflow as tf

FLAGS = None


def main(_):  # pylint: disable=unused-argument

    # Step 4: Compare performance of the pruned and the unpruned net
    print("%%%% Evaluate Base Model%%%%%%")
    tf.reset_default_graph()
    base_model_path = os.path.join(FLAGS.models_dir, 'base/')
    train_cifar.validation(FLAGS, base_model_path)
    
    print("%%%% Evaluate Pruned Model%%%%%%")
    tf.reset_default_graph()
    pruned_model_path = os.path.join(FLAGS.models_dir, 'pruned/')
    train_cifar.validation(FLAGS, pruned_model_path)


#if __name__ == '__main__':
if True:
  parser = argparse.ArgumentParser()
  
  parser.add_argument(
      '--max_steps',
      type=int,
      default=20,
      help='Number of steps'
  )
  parser.add_argument(
      '--finetune_steps',
      type=int,
      default=20,
      help='Number of steps'
  )
  parser.add_argument(
      '--batch_size',
      type=int,
      default=16,
      help='Batch size.'
  )
  parser.add_argument(
      '--learning_rate',
      type=float,
      default=0.01,
      help='learning rate'
  )
  parser.add_argument(
      '--learning_rate_finetune',
      type=float,
      default=0.00001,
      help='finetune learning rate'
  )
  parser.add_argument(
      '--train_dir',
      type=str,
      default='/tmp/resnet_train',
      help='Directory where to write event logs and checkpoints.'
  )  
  parser.add_argument(
      '--models_dir',
      type=str,
      default='/tmp/models_dir',
      help='Directory to save the models to compare'
  )
  parser.add_argument(
      '--data_dir',
      type=str,
      default='/tmp/cifar-data',
      help='Directory holding the data'
  )
  parser.add_argument(
      '--resume',
      type=bool,
      default=False,
      help='resume from latest saved state'
  )
  parser.add_argument(
      '--minimal_summaries',
      type=bool,
      default=True,
      help='produce fewer summaries to save HD space'
  )
  parser.add_argument(
      '--use_bn',
      type=bool,
      default=True,
      help='use batch normalization if true, else use biases'
  )
  parser.add_argument(
      '--threshold_weights',
      type=float,
      default='0.05',
      help='threshold for weights'
  )

  
  FLAGS, unparsed = parser.parse_known_args()
  print("FLAGS:")
  for k in FLAGS.__dict__:
      print("{} : {}".format(k, FLAGS.__dict__[k]))
  print("\n")
  
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)

