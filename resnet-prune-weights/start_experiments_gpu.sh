#Submit to cluster to gpu
FILE=$1
DATE=$(date +%d%m%Y_%H%M%S)
DATA_DIR=/srv/glusterfs/muellerp/tmp/cifar-data
MODEL_DIR=/srv/glusterfs/muellerp/tmp/models_dir
TRAIN_DIR=/srv/glusterfs/muellerp/tmp/resnet-train
MAX_STEPS=64000
THRESHOLD=0.1
FINETUNE_STEPS=16000
FINETUNE_LEARNING_RATE=0.01
NAME=TH${THRESHOLD}_${DATE}_${FILE}
BATCH_SIZE=128
export NAME

if [ $# -lt 1 ]
  then
    echo "Please provide file name"
else

echo "removing existing models ..."
if [ 1 -lt 0 ]
  then
	rm -r /srv/glusterfs/muellerp/tmp/models_dir/base/*
	rm -r /srv/glusterfs/muellerp/tmp/models_dir/pruned/*
	rm -r /srv/glusterfs/muellerp/tmp/models_dir/final_models/base/*
	rm -r /srv/glusterfs/muellerp/tmp/models_dir/final_models/pruned/*
fi
echo "submitting job to cluster..."
  qsub -N $NAME -l gpu ./pywrap_gpu.sh $FILE --models_dir $MODEL_DIR --data_dir $DATA_DIR --train_dir $TRAIN_DIR --max_steps $MAX_STEPS --threshold_weights $THRESHOLD --finetune_steps $FINETUNE_STEPS --learning_rate_finetune $FINETUNE_LEARNING_RATE --batch_size $BATCH_SIZE
fi
